# builder

This project is generated with [yo angular generator](https://github.com/yeoman/generator-angular)
version 0.12.1.

## Demo
* [Builder](http://builder.datatablar.com/#/).
* [Users management](http://datatablar.com/#/)
* [Authentication and permissions](http://demo-auth.datatablar.com/#/)
* [Proxy - Wrapper your APIs](http://demo-proxy.datatablar.com/#/)
* [Film database](http://demo-film.datatablar.com/#/)

## Build & development

Run `grunt` for building and `grunt serve` for preview.

## Userguide

Check out [here](http://userguide.datatablar.com/).