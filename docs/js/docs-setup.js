NG_DOCS={
  "sections": {
    "api": "API Documentation"
  },
  "pages": [
    {
      "section": "api",
      "id": "builderApp",
      "shortName": "builderApp",
      "type": "overview",
      "moduleName": "builderApp",
      "shortDescription": "builderApp",
      "keywords": "api application builderapp main module overview"
    },
    {
      "section": "api",
      "id": "builderApp.controller:BuilderController",
      "shortName": "BuilderController",
      "type": "controller",
      "moduleName": "builderApp",
      "shortDescription": "BuilderController",
      "keywords": "$http $rootscope $scope $window activate adapted allows api appear apply applymodel brower browser builder builderapp buildercontroller call called code column columnindex components configuration configurations controller copy creating current currently data-tablar decoded delete demo dialog display displayed dttb_builder_menu dttbmodel easily editor encode encoding essential field filter firt gotopage init initcreatingfieldfromcolumn initial initreadingfilterfromcolumn inside javascript key keyword left load loaded local localstorageservice menu method modify module named navigate navigation ngdialog notification object pagekey panel paste preview previewcodes previewresult process produce properties property re-used reading reopenrecent representing result save saveconfiguration select service source-code specific storage stored switch table type upload uploadcode user variable viewmodel wishes"
    },
    {
      "section": "api",
      "id": "builderApp.controller:builderFormConversionRequestController",
      "shortName": "builderFormConversionRequestController",
      "type": "controller",
      "moduleName": "builderApp",
      "shortDescription": "builderFormConversionRequest",
      "keywords": "api builderapp builderformconversionrequest controller directive"
    },
    {
      "section": "api",
      "id": "builderApp.controller:builderFormConversionResponseController",
      "shortName": "builderFormConversionResponseController",
      "type": "controller",
      "moduleName": "builderApp",
      "shortDescription": "builderFormConversionResponse",
      "keywords": "api builderapp builderformconversionresponse controller directive"
    },
    {
      "section": "api",
      "id": "builderApp.controller:builderFormLabelController",
      "shortName": "builderFormLabelController",
      "type": "controller",
      "moduleName": "builderApp",
      "shortDescription": "builderFormLabel",
      "keywords": "$scope api builderapp builderformlabel controller directive"
    },
    {
      "section": "api",
      "id": "builderApp.controller:builderFormTypeConfigController",
      "shortName": "builderFormTypeConfigController",
      "type": "controller",
      "moduleName": "builderApp",
      "shortDescription": "builderFormTypeConfig",
      "keywords": "api builderapp builderformtypeconfig controller directive"
    },
    {
      "section": "api",
      "id": "builderApp.controller:builderFormValidationController",
      "shortName": "builderFormValidationController",
      "type": "controller",
      "moduleName": "builderApp",
      "shortDescription": "builderFormValidation",
      "keywords": "$scope api builderapp builderformvalidation controller directive service"
    },
    {
      "section": "api",
      "id": "builderApp.controller:builderFormYesNoController",
      "shortName": "builderFormYesNoController",
      "type": "controller",
      "moduleName": "builderApp",
      "shortDescription": "builderFormYesNo",
      "keywords": "api builderapp builderformyesno controller directive"
    },
    {
      "section": "api",
      "id": "builderApp.controller:builderHttpConfigController",
      "shortName": "builderHttpConfigController",
      "type": "controller",
      "moduleName": "builderApp",
      "shortDescription": "builderHttpConfig",
      "keywords": "$scope api builderapp builderhttpconfig controller directive helperservice"
    },
    {
      "section": "api",
      "id": "builderApp.controller:builderModalHelperController",
      "shortName": "builderModalHelperController",
      "type": "controller",
      "moduleName": "builderApp",
      "shortDescription": "builderModalHelper",
      "keywords": "$rootscope $scope api builderapp buildermodalhelper buildermodalhelpercontroller controller dialog directive display displayhelp dttb_builder_labels help main method ngdialog"
    },
    {
      "section": "api",
      "id": "builderApp.controller:builderShowOptionAjaxRequestsController",
      "shortName": "builderShowOptionAjaxRequestsController",
      "type": "controller",
      "moduleName": "builderApp",
      "shortDescription": "builderShowOptionAjaxRequests",
      "keywords": "$rootscope $scope api builderapp buildershowoptionajaxrequests controller directive"
    },
    {
      "section": "api",
      "id": "builderApp.controller:builderToggleVarController",
      "shortName": "builderToggleVarController",
      "type": "controller",
      "moduleName": "builderApp",
      "shortDescription": "builderToggleVar",
      "keywords": "$rootscope $scope api builderapp buildertogglevar controller directive enable main method ngdialog service toggle variable"
    },
    {
      "section": "api",
      "id": "builderApp.controller:dttbBuilderMenuController",
      "shortName": "dttbBuilderMenuController",
      "type": "controller",
      "moduleName": "builderApp",
      "shortDescription": "dttbBuilderMenu",
      "keywords": "$scope api builderapp controller directive dttbbuildermenu helperservice"
    },
    {
      "section": "api",
      "id": "builderApp.directive:builderConvertToNumber",
      "shortName": "builderConvertToNumber",
      "type": "directive",
      "moduleName": "builderApp",
      "shortDescription": "Convert ng-model from string to number. For example in select form element.",
      "keywords": "api builderapp convert directive element example form ng-model number select string"
    },
    {
      "section": "api",
      "id": "builderApp.directive:builderFormConversionRequest",
      "shortName": "builderFormConversionRequest",
      "type": "directive",
      "moduleName": "builderApp",
      "shortDescription": "Display a code editor for that user can use to write a conversion-request function.",
      "keywords": "api binding builderapp builderconversionmodel code conversion conversion-request directive display editor form function model request required user write"
    },
    {
      "section": "api",
      "id": "builderApp.directive:builderFormConversionResponse",
      "shortName": "builderFormConversionResponse",
      "type": "directive",
      "moduleName": "builderApp",
      "shortDescription": "Display a code editor for that user can use to write a conversion-response function.",
      "keywords": "api binding builderapp builderconversionmodel code conversion conversion-response directive display editor form function model required response user write"
    },
    {
      "section": "api",
      "id": "builderApp.directive:builderFormLabel",
      "shortName": "builderFormLabel",
      "type": "directive",
      "moduleName": "builderApp",
      "shortDescription": "Display a label for each form element.",
      "keywords": "api attribute builderapp builderlabelkey directive display element form key label parameters required"
    },
    {
      "section": "api",
      "id": "builderApp.directive:builderFormTypeConfig",
      "shortName": "builderFormTypeConfig",
      "type": "directive",
      "moduleName": "builderApp",
      "shortDescription": "Display a form type configuration",
      "keywords": "api attribute bind builderapp builderformtypeconfig builderlabelkey configuration directive display form required type"
    },
    {
      "section": "api",
      "id": "builderApp.directive:builderFormValidation",
      "shortName": "builderFormValidation",
      "type": "directive",
      "moduleName": "builderApp",
      "shortDescription": "Display a form element for validations.",
      "keywords": "api array binding builderapp builderdatatype buildervalidations data directive display element form model required type validations"
    },
    {
      "section": "api",
      "id": "builderApp.directive:builderFormYesNo",
      "shortName": "builderFormYesNo",
      "type": "directive",
      "moduleName": "builderApp",
      "shortDescription": "Display a yes-no toggle button for boolean variable..",
      "keywords": "api binding boolean builderapp builderformmodel button directive display element form model required toggle variable yes-no"
    },
    {
      "section": "api",
      "id": "builderApp.directive:builderHttpConfig",
      "shortName": "builderHttpConfig",
      "type": "directive",
      "moduleName": "builderApp",
      "shortDescription": "Display a list of form elements for HTTP Configuration",
      "keywords": "api binding builderapp configuration directive display dttbhttpconfig elements form http list model required"
    },
    {
      "section": "api",
      "id": "builderApp.directive:builderModalHelper",
      "shortName": "builderModalHelper",
      "type": "directive",
      "moduleName": "builderApp",
      "shortDescription": "Display a help dialog for a component.",
      "keywords": "api binding builderapp component dialog directive display help helpkey required"
    },
    {
      "section": "api",
      "id": "builderApp.directive:builderShowOptionAjaxRequests",
      "shortName": "builderShowOptionAjaxRequests",
      "type": "directive",
      "moduleName": "builderApp",
      "shortDescription": "Show modal - list of all options ajax requests",
      "keywords": "ajax api builderapp directive list modal options requests"
    },
    {
      "section": "api",
      "id": "builderApp.directive:builderToggleVar",
      "shortName": "builderToggleVar",
      "type": "directive",
      "moduleName": "builderApp",
      "shortDescription": "Display a toggle button to enable/disable a propery/variable.",
      "keywords": "api binding builderapp buildertogglekey buildertogglemodel button directive display enable propery required toggle"
    },
    {
      "section": "api",
      "id": "builderApp.directive:dttbBuilderMenu",
      "shortName": "dttbBuilderMenu",
      "type": "directive",
      "moduleName": "builderApp",
      "shortDescription": "Display a left menu of builder.",
      "keywords": "api attribute binding builder builderapp directive display dttbmenu dttbmenulevel dttbrootlevel left menu optional required"
    },
    {
      "section": "api",
      "id": "builderApp.service:helperService",
      "shortName": "helperService",
      "type": "service",
      "moduleName": "builderApp",
      "shortDescription": "Service provides useful helper functions.",
      "keywords": "adapt add addbasicobjecttoarray addtoarray addtoarraywithtemplate addtoobject api array arrayreference asarray boolean builder builderapp change code configuration conversionrequest conversionrequestbody conversionresponse conversionresponsebody convert convertarraytoobject converted convertobjecttoarray data data-tablar decode decoded decoding default defined delete directive dttb_builder_parameters dttbbuilder dttbbuilderactive dttbbuildermenu dttbmenulevel dttbmodel encode encoded escape escaped escapequotestring extend function functions generate generatedcodes generateiteration generatetabs getvartype handle headers helper host indextoremove javascript key level list menu method model module ngdialog number numrowsperpageoptions obj object objectreference options pair pairs params parsed parsehosturl path position post-processing postdecode property propertyname propertytoremove propery quote recursive reference remove removed removefromarray removefromobject result service source str string tab template templatekey toggleoffplaceholder type url urlstring valuetoadd variable"
    }
  ],
  "apis": {
    "api": true
  },
  "html5Mode": false,
  "editExample": true,
  "startPage": "/api",
  "scripts": [
    "angular.min.js"
  ]
};