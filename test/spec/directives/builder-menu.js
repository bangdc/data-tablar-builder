'use strict';

describe('testing dttb-builder-menu directive', function() {
    var compile, directiveElem, scope;

    beforeEach(function() {
        angular.mock.module('builderApp');

        inject(function($compile, $rootScope, $httpBackend, dttbMenuHelper) {
        	$httpBackend.whenGET(/^\/languages\//).passThrough();
            compile = $compile;
            scope = $rootScope.$new();
        });
        scope.mockMenu0 = [];
        scope.mockMenu = [{
            title: '1',
            description: '',
            active: false,
            templateUrl: '1.html'
        }, {
            title: '2',
            description: '',
            active: false,
            templateUrl: '2.html'
        }, {
            title: '3',
            description: '',
            active: false,
            templateUrl: '3.html',
            subs: [{
                title: '3.1',
                description: '',
                active: false,
                templateUrl: '3.1.html'
            }, {
                title: '3.2',
                description: '',
                active: false,
                templateUrl: '3.2.html',
                subs: [{
                    title: '3.2.1',
                    description: '',
                    active: false,
                    templateUrl: '3.2.1.html'
                }, {
                    title: '3.2.2',
                    description: '',
                    active: false,
                    templateUrl: '3.2.2.html',
                }]
            }]
        }];
        directiveElem = getCompiledElement();
    });

    function getCompiledElement() {
        var element = angular.element('<ul></ul>');
        var compiledElement = compile(element)(scope);
        scope.$digest();
        return compiledElement;
    }


    it('dttbMenu on isolated scope should be two-way bound', function() {
        //var isolatedScope = directiveElem.isolateScope();

        //isolatedScope.config.prop = "value2";

        expect(1).toEqual(1);
    });
});
