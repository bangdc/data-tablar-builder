'use strict';

describe('Testing BuilderController', function() {

    // load the controller's module
    beforeEach(module('builderApp'));

    var BuilderController,
        scope = {};

    // Initialize the controller and a mock scope
    beforeEach(inject(function($controller, $rootScope) {
        scope = $rootScope.$new();
        BuilderController = $controller('BuilderController', {
            $scope: scope
        });
    }));

    it('should be defined', function() {
      expect(BuilderController).toBeDefined();
    });
});
