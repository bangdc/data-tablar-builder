'use strict';

describe('testing helper service', function() {
    beforeEach(function() {
        angular.mock.module('builderApp');
    });

    var helper;

    beforeEach(inject(function(_helper_) {
        helper = _helper_;
    }));

    it('should be defined', function() {
        expect(helper).toBeDefined();
    });

    describe('testing method helper.convertArrayToObject', function() {
        var foo = [{
            key: 'foo',
            value: 'bar'
        }, {
            key: 'bar',
            value: 'foo',
        }]
        it('should be defined the method helper.convertArrayToObject', function() {
            expect(helper.convertArrayToObject).toBeDefined();
        });

        it('should convert correctly array of objects (key, value) to pure object', function() {
            var bar = {
                foo: 'bar',
                bar: 'foo'
            };
            expect(helper.convertArrayToObject(foo)).toEqual(bar);
        });

        it('should convert non-array to empty-object {}', function() {
            expect(helper.convertArrayToObject(1)).toEqual({});
        });

        it('should convert wrong-formated-array to empty-object {}', function() {
            expect(helper.convertArrayToObject([{
                a: 'a',
                b: 'b',
            }])).toEqual({});
        });

        it('should convert empty-array [] to empty-object {}', function() {
            expect(helper.convertArrayToObject([])).toEqual({});
        });
    });

    describe('testing method helper.getVarType:', function() {
        it('should be defined the method helper.getVarType', function() {
            expect(helper.getVarType).toBeDefined();
        });

        it('should be string', function() {
            expect(helper.getVarType('string')).toEqual('string');
        });

        it('should be number', function() {
            expect(helper.getVarType(5)).toEqual('number');
        });

        it('should be array', function() {
            expect(helper.getVarType([{ a: 'a' }, { b: 'b' }])).toEqual('array');
        });

        it('should be object', function() {
            expect(helper.getVarType({ a: 'a' })).toEqual('object');
        });

        it('should be boolean', function() {
            expect(helper.getVarType(true)).toEqual('boolean');
        });

        it('should be undefined', function() {
            var person;
            expect(helper.getVarType(person)).toEqual('undefined');
        });

        it('should be null', function() {
            expect(helper.getVarType(null)).toEqual('null');
        });
    });

    describe('testing method helper.escapeQuoteString:', function() {
        it('should be defined the method helper.escapeQuoteString', function() {
            expect(helper.escapeQuoteString).toBeDefined();
        });

        var foo = ['"bar"', "'bar'", '"bar', "bar'", 'foo'];

        it('should be bar', function() {
            expect(helper.escapeQuoteString(foo[0])).toEqual('bar');
        });

        it('should be bar', function() {
            expect(helper.escapeQuoteString(foo[1])).toEqual('bar');
        });

        it('should be bar', function() {
            expect(helper.escapeQuoteString(foo[2])).toEqual('bar');
        });

        it('should be bar', function() {
            expect(helper.escapeQuoteString(foo[3])).toEqual('bar');
        });

        it('should be foo', function() {
            expect(helper.escapeQuoteString(foo[4])).toEqual('foo');
        });
    });

    describe('testing method helper.generateObjectToCodeIteration:', function() {
        it('should be defined the method helper.generateObjectToCodeIteration', function() {
            expect(helper.generateObjectToCodeIteration).toBeDefined();
        });
    });
});
