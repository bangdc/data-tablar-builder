describe('builderApp.controller:BuilderController', function() {
    var result;
    var target;

    var httpSpy;
    var scopeSpy;
    var helperSpy;
    var httpServiceSpy;
    var localStorageServiceSpy;
    var ngDialogSpy;

    var httpServiceCallSpy;
    var ngDialogOpenConfirmSpy;

    beforeEach(module('.'));

    beforeEach(function() {
        httpSpy = jasmine.createSpyObj('$http', ['']);

        scopeSpy = jasmine.createSpyObj('$scope', ['$watch']);

        helperSpy = jasmine.createSpyObj('helper', ['generateObjectToCodeIteration']);

        httpServiceSpy = jasmine.createSpyObj('httpService', ['call']);

        httpServiceCallSpy = jasmine.createSpyObj('httpServiceSpy.call', ['then']);
        httpServiceSpy.call.and.returnValue(httpServiceCallSpy);

        localStorageServiceSpy = jasmine.createSpyObj('localStorageService', ['get', 'set']);

        ngDialogSpy = jasmine.createSpyObj('ngDialog', ['open', 'openConfirm']);

        ngDialogOpenConfirmSpy = jasmine.createSpyObj('ngDialogSpy.openConfirm', ['then']);
        ngDialogSpy.openConfirm.and.returnValue(ngDialogOpenConfirmSpy);
    });

    function construct() {
        inject(function($controller) {
            target = $controller('builderApp.controller:BuilderController', {
                $http: httpSpy,
                $scope: scopeSpy,
                helper: helperSpy,
                httpService: httpServiceSpy,
                localStorageService: localStorageServiceSpy,
                ngDialog: ngDialogSpy,
            });
        });
    }

    /**
     * addResponsePath
     */

    describe('When being addResponsePath', function() {
        beforeEach(function() {
            construct();

            target.addResponsePath();
        });

        it('should ', function() {

        });
    });

    /**
     * removeResponsePath
     */

    describe('When being removeResponsePath', function() {
        beforeEach(function() {
            construct();

            target.removeResponsePath();
        });

        it('should ', function() {

        });
    });

    /**
     * toggleDaPaHe
     */

    describe('When being toggleDaPaHe', function() {
        beforeEach(function() {
            construct();

            target.toggleDaPaHe();
        });

        it('should ', function() {

        });
    });

    /**
     * initFromColumnsToCreating
     */

    describe('When being initFromColumnsToCreating', function() {
        beforeEach(function() {
            construct();

            target.initFromColumnsToCreating();
        });

        it('should ', function() {

        });
    });

    /**
     * initFromColumnsToFilters
     */

    describe('When being initFromColumnsToFilters', function() {
        beforeEach(function() {
            construct();

            target.initFromColumnsToFilters();
        });

        it('should ', function() {

        });
    });

    /**
     * removeReadingFilterCondition
     */

    describe('When being removeReadingFilterCondition', function() {
        beforeEach(function() {
            construct();

            target.removeReadingFilterCondition();
        });

        it('should ', function() {

        });
    });

    /**
     * addReadingFilterCondition
     */

    describe('When being addReadingFilterCondition', function() {
        beforeEach(function() {
            construct();

            target.addReadingFilterCondition();
        });

        it('should ', function() {

        });
    });

    /**
     * toggleReadingFilterConditions
     */

    describe('When being toggleReadingFilterConditions', function() {
        beforeEach(function() {
            construct();

            target.toggleReadingFilterConditions();
        });

        it('should ', function() {

        });
    });

    /**
     * removeReadingFilter
     */

    describe('When being removeReadingFilter', function() {
        beforeEach(function() {
            construct();

            target.removeReadingFilter();
        });

        it('should ', function() {

        });
    });

    /**
     * collapseReadingFilter
     */

    describe('When being collapseReadingFilter', function() {
        beforeEach(function() {
            construct();

            target.collapseReadingFilter();
        });

        it('should ', function() {

        });
    });

    /**
     * addReadingFilter
     */

    describe('When being addReadingFilter', function() {
        beforeEach(function() {
            construct();

            target.addReadingFilter();
        });

        it('should ', function() {

        });
    });

    /**
     * toggleReadingFilters
     */

    describe('When being toggleReadingFilters', function() {
        beforeEach(function() {
            construct();

            target.toggleReadingFilters();
        });

        it('should ', function() {

        });
    });

    /**
     * removeValidationFromCreatingField
     */

    describe('When being removeValidationFromCreatingField', function() {
        beforeEach(function() {
            construct();

            target.removeValidationFromCreatingField();
        });

        it('should ', function() {

        });
    });

    /**
     * addValidationToCreatingField
     */

    describe('When being addValidationToCreatingField', function() {
        beforeEach(function() {
            construct();

            target.addValidationToCreatingField();
        });

        it('should ', function() {

        });
    });

    /**
     * removeCreatingField
     */

    describe('When being removeCreatingField', function() {
        beforeEach(function() {
            construct();

            target.removeCreatingField();
        });

        it('should ', function() {

        });
    });

    /**
     * collapseCreatingField
     */

    describe('When being collapseCreatingField', function() {
        beforeEach(function() {
            construct();

            target.collapseCreatingField();
        });

        it('should ', function() {

        });
    });

    /**
     * addCreatingFieldForm
     */

    describe('When being addCreatingFieldForm', function() {
        beforeEach(function() {
            construct();

            target.addCreatingFieldForm();
        });

        it('should ', function() {

        });
    });

    /**
     * toggleConversionOfAction
     */

    describe('When being toggleConversionOfAction', function() {
        beforeEach(function() {
            construct();

            target.toggleConversionOfAction();
        });

        it('should ', function() {

        });
    });

    /**
     * toggleAction
     */

    describe('When being toggleAction', function() {
        beforeEach(function() {
            construct();

            target.toggleAction();
        });

        it('should ', function() {

        });
    });

    /**
     * addColumnHighlight
     */

    describe('When being addColumnHighlight', function() {
        beforeEach(function() {
            construct();

            target.addColumnHighlight();
        });

        it('should ', function() {

        });
    });

    /**
     * addValidationToColumn
     */

    describe('When being addValidationToColumn', function() {
        beforeEach(function() {
            construct();

            target.addValidationToColumn();
        });

        it('should ', function() {

        });
    });

    /**
     * removeValidationFromColumn
     */

    describe('When being removeValidationFromColumn', function() {
        beforeEach(function() {
            construct();

            target.removeValidationFromColumn();
        });

        it('should ', function() {

        });
    });

    /**
     * addColumnForm
     */

    describe('When being addColumnForm', function() {
        beforeEach(function() {
            construct();

            target.addColumnForm();
        });

        it('should ', function() {

        });
    });

    /**
     * removeColumnForm
     */

    describe('When being removeColumnForm', function() {
        beforeEach(function() {
            construct();

            target.removeColumnForm();
        });

        it('should ', function() {

        });
    });

    /**
     * collapseColumnForm
     */

    describe('When being collapseColumnForm', function() {
        beforeEach(function() {
            construct();

            target.collapseColumnForm();
        });

        it('should ', function() {

        });
    });

    /**
     * help
     */

    describe('When being help', function() {
        beforeEach(function() {
            construct();

            target.help();
        });

        it('should ', function() {

        });
    });

    /**
     * changeAjaxConfigOptions
     */

    describe('When being changeAjaxConfigOptions', function() {
        beforeEach(function() {
            construct();

            target.changeAjaxConfigOptions();
        });

        it('should ', function() {

        });
    });

    /**
     * init
     */

    describe('When being init', function() {
        beforeEach(function() {
            construct();

            target.init();
        });

        it('should ', function() {

        });
    });

    /**
     * removePropertyFromObject
     */

    describe('When being removePropertyFromObject', function() {
        beforeEach(function() {
            construct();

            target.removePropertyFromObject();
        });

        it('should ', function() {

        });
    });

    /**
     * addPropertyToObject
     */

    describe('When being addPropertyToObject', function() {
        beforeEach(function() {
            construct();

            target.addPropertyToObject();
        });

        it('should ', function() {

        });
    });

    /**
     * testRequest
     */

    describe('When being testRequest', function() {
        beforeEach(function() {
            construct();

            target.testRequest();
        });

        it('should ', function() {

        });
    });

    /**
     * activateStep
     */

    describe('When being activateStep', function() {
        beforeEach(function() {
            construct();

            target.activateStep();
        });

        it('should ', function() {

        });
    });

    /**
     * generateObjectToCode
     */

    describe('When being generateObjectToCode...', function() {
        beforeEach(function() {
            construct();

            target.generateObjectToCode();
        });

        it('should ', function() {

        });
    });

    /**
     * writeObjectToFile
     */

    describe('When being writeObjectToFile', function() {
        beforeEach(function() {
            construct();

            target.writeObjectToFile();
        });

        it('should ', function() {

        });
    });
});
