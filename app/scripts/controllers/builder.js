'use strict';
/**
 * @ngdoc controller
 * @name builderApp.controller:BuilderController
 * 
 * @description
 * # BuilderController
 * Controller of the builderApp
 *  
 * @requires ngDialog
 * @requires Notification
 * @requires $http
 * @requires $window
 * @requires builderApp.service:helperService
 * @requires localStorageService
 * @requires $scope
 * @requires $rootScope
 * @requires DTTB_BUILDER_MENU
 **/

angular.module('builderApp')
    .controller('BuilderController', BuilderController);

BuilderController.$inject = [
    'ngDialog',
    'Notification',
    '$http',
    '$window',
    'helperService',
    'localStorageService',
    '$scope',
    '$rootScope',
    'DTTB_BUILDER_MENU'
];

function BuilderController(
    ngDialog,
    Notification,
    $http,
    $window,
    helperService,
    localStorageService,
    $scope,
    $rootScope,
    DTTB_BUILDER_MENU
) {
    /* jshint validthis: true */
    /**
     * @ngdoc property
     * @name builderApp.controller.BuilderController#builder
     * @propertyOf builderApp.controller:BuilderController
     * @description A named variable for the `this` keyword representing the ViewModel
     **/
    var builder = this;
    builder.menu = DTTB_BUILDER_MENU;

    builder.currentConfigTimestamp = new Date().getTime();

    $scope.$on('dttbBuilderMenuTemplateUrl', function(event, templateUrl) {
        builder.templateUrl = templateUrl;
        if (templateUrl === 'builder-5-preview.html') {
            builder.previewCodes();
        }
        if (templateUrl === 'builder-6-results.html') {
            builder.previewResult();
        }
    });

    $scope.$on('update-option-ajax-request', function(event, requestIndex, optionAjaxRequest) {
        builder.dttbModel.optionAjaxRequests[requestIndex] = angular.copy(optionAjaxRequest);
    });

    $scope.$on('new-option-ajax-request', function(event, optionAjaxRequest) {
        builder.dttbModel.optionAjaxRequests.push(optionAjaxRequest);
    });
    // Prepare dttbModel template
    //builder.dttbModel = angular.copy($rootScope.dttbBuilderParameters.tempDttbModel);
    applyModel($rootScope.dttbBuilderParameters.tempDttbModel);

    builder.addToArrayWithTemplate = helperService.addToArrayWithTemplate;
    builder.addToArray = helperService.addToArray;
    builder.removeFromArray = helperService.removeFromArray;
    builder.addToObject = helperService.addToObject;
    builder.removeFromObject = helperService.removeFromObject;
    builder.addBasicObjectToArray = helperService.addBasicObjectToArray;

    builder.previewCodes = previewCodes;

    builder.goToPage = goToPage;

    builder.previewResult = previewResult;

    builder.uploadCode = uploadCode;
    builder.reOpenRecent = reOpenRecent;
    builder.saveConfiguration = saveConfiguration;

    builder.applyModel = applyModel;

    builder.initCreatingFieldFromColumn = initCreatingFieldFromColumn;
    builder.initReadingFilterFromColumn = initReadingFilterFromColumn;

    init();

    $window.onbeforeunload = function(e) {
        return true;
    };

    /**
     * @ngdoc method 
     * @name initCreatingFieldFromColumn
     * @methodOf builderApp.controller:BuilderController
     *
     * @param {object} field The current creating field.
     * @param {number} columnIndex Index of column to copy
     * 
     * @description
     * Apply some essential properties of a column to creating field.
     * 
     */
    function initCreatingFieldFromColumn(field, columnIndex) {
        var scope = $rootScope.$new();
        scope.field = field;
        scope.column = builder.dttbModel.columns[columnIndex];
        scope.confirmMessage = 'All the current properties of this field will be overridden by the properties of the column. Are you sure?';
        scope.hasDefinedConfirmDialog = true;
        scope.confirmDialog = function() {
            try {
                scope.field.key = angular.copy(scope.column.key);
                scope.field.label = angular.copy(scope.column.label);
                scope.field.dataType = angular.copy(scope.column.dataType);
                scope.field.formType = angular.copy(scope.column.formType);
                scope.field.formTypeConfig = angular.copy(scope.column.formTypeConfig);
                scope.field.validations = angular.copy(scope.column.validations);
                ngDialog.close();
            } catch (e) {
                console.warn(e, 'Error happened while initializing field from column');
                Notification.error('Error happened while initializing field from column');
            }
        };
        ngDialog.openConfirm({
            scope: scope,
            template: '/views/modal-confirm.html',
        });
    }

    /**
     * @ngdoc method 
     * @name initReadingFilterFromColumn
     * @methodOf builderApp.controller:BuilderController
     *
     * @param {object} filter The current reading filter.
     * @param {number} columnIndex Index of column to copy
     * 
     * @description
     * Apply some essential properties of a column to reading filter.
     * 
     */
    function initReadingFilterFromColumn(filter, columnIndex) {
        var scope = $rootScope.$new();
        scope.filter = filter;
        scope.hasDefinedConfirmDialog = true;
        scope.column = builder.dttbModel.columns[columnIndex];
        scope.confirmMessage = 'All the current properties of this filter will be overridden by the properties of the column. Are you sure?';
        scope.confirmDialog = function() {
            try {
                scope.filter.key = angular.copy(scope.column.key);
                scope.filter.label = angular.copy(scope.column.label);
                ngDialog.close();
            } catch (e) {
                console.warn(e, 'Error happened while initializing filter from column');
                Notification.error('Error happened while initializing filter from column');
            }
        };
        ngDialog.openConfirm({
            scope: scope,
            template: '/views/modal-confirm.html',
        });
    }

    /**
     * @ngdoc method 
     * @name applyModel
     * @methodOf builderApp.controller:BuilderController
     * 
     * @description
     * Method to apply a configuration to current dttbModel.
     * 
     */
    function applyModel(model) {
        builder.dttbModel = angular.copy(model);
        $rootScope.dttbBuilderOptionAjaxRequests = builder.dttbModel.optionAjaxRequests;
        //delete model;
    }

    /**
     * @ngdoc method 
     * @name saveConfiguration
     * @methodOf builderApp.controller:BuilderController
     * 
     * @description
     * Method to save the current configuration to local storage of browser.
     * This configuration can be loaded and re-used later if user wishes to do so.
     * 
     */
    function saveConfiguration() {
        var recentConfigurations = localStorageService.get('configurations');
        if (!angular.isArray(recentConfigurations)) {
            recentConfigurations = [];
        }
        var scope = $rootScope.$new();
        scope.configId = angular.copy(builder.dttbModel.table.label);

        if (recentConfigurations.length) {
            angular.forEach(recentConfigurations, function(configuration, index) {
                if (configuration.time === builder.currentConfigTimestamp) {
                    scope.configId = angular.copy(configuration.id);
                    scope.alreadySavedIndex = index;
                    scope.alreadySaved = true;
                }
            });
        }

        scope.confirmDialog = function(configId) {
            var currentTimestamp = new Date().getTime();
            var configurationToSave = {
                id: configId,
                model: builder.dttbModel,
                time: currentTimestamp
            };

            if (scope.hasOwnProperty('alreadySavedIndex') &&
                recentConfigurations[scope.alreadySavedIndex].time === builder.currentConfigTimestamp) {
                recentConfigurations[scope.alreadySavedIndex] = angular.copy(configurationToSave);
            } else {
                recentConfigurations.unshift(configurationToSave);
                if (recentConfigurations.length > 10) {
                    recentConfigurations.pop();
                }
            }

            builder.currentConfigTimestamp = currentTimestamp;
            localStorageService.set('configurations', recentConfigurations);
            ngDialog.closeAll();
        };
        ngDialog.openConfirm({
            scope: scope,
            template: '/views/modal-save-configuration.html',
        });
    }

    /**
     * @ngdoc method 
     * @name reOpenRecent
     * @methodOf builderApp.controller:BuilderController
     * 
     * @description
     * Method to load the configurations, which are currently stored in
     * the local storage of brower. There are 10 recent configurations.
     * A dialog will be displayed and let user to select or delete
     * a specific configuration.
     * 
     */
    function reOpenRecent() {
        var scope = $rootScope.$new();
        scope.recentConfigurations = localStorageService.get('configurations');
        angular.forEach(scope.recentConfigurations, function(configuration) {
            var now = new Date();
            var seconds = ((now.getTime() - configuration.time) * .001) >> 0;
            var minutes = seconds / 60;
            var hours = minutes / 60;
            var days = hours / 24;
            var years = days / 365;

            if (seconds < 60) {
                configuration.timeAgo = Math.abs(Math.round(seconds)) + ' secs';
            } else {
                if (minutes < 60) {
                    configuration.timeAgo = Math.abs(Math.round(minutes)) + ' mins';
                } else {
                    if (hours < 24) {
                        configuration.timeAgo = Math.abs(Math.round(hours)) + ' hours';
                    } else {
                        if (days < 365) {
                            configuration.timeAgo = Math.abs(Math.round(days)) + ' days';
                        } else {
                            configuration.timeAgo = Math.abs(Math.round(years)) + ' years';
                        }
                    }
                }
            }
        });
        scope.applyConfig = function(config, timestamp) {
            var subScope = scope.$new();
            subScope.hasDefinedConfirmDialog = true;
            subScope.confirmMessage = "This will override the current configuration. Are you sure?";
            subScope.confirmDialog = function() {
                try {
                    //builder.dttbModel = angular.copy(config);
                    applyModel(config);
                    builder.currentConfigTimestamp = timestamp;
                    ngDialog.closeAll();
                } catch (e) {
                    console.warn(e, 'Error while applying the saved configuration.');
                    Notification.error('Error while applying the saved configuration.');
                }
            };
            ngDialog.openConfirm({
                scope: subScope,
                template: '/views/modal-confirm.html',
            });
        };
        scope.deleteConfig = function(configIndex) {
            ngDialog.openConfirm({
                template: '/views/modal-confirm.html',
            }).then(function() {
                scope.recentConfigurations.splice(configIndex, 1);
                var storageConfigurations = localStorageService.get('configurations');
                storageConfigurations.splice(configIndex, 1);
                localStorageService.set('configurations', storageConfigurations);
            });
        };
        scope.clearAll = function() {
            ngDialog.openConfirm({
                template: '/views/modal-confirm.html',
            }).then(function() {
                localStorageService.set('configurations', []);
                ngDialog.closeAll();
            });
        };
        ngDialog.openConfirm({
            scope: scope,
            template: '/views/modal-recent-configurations.html',
        });
    }

    /**
     * @ngdoc method 
     * @name uploadCode
     * @methodOf builderApp.controller:BuilderController
     * 
     * @description
     * Method allows user to upload a source-code configuration.
     * A code editor will appear so user can easily paste an old 
     * configuration into it. 
     * The source-code will then be decoded and adapted to builder.
     * Therefore, user can easily to use builder to modify that configuration.
     */
    function uploadCode() {
        var scope = $rootScope.$new();
        scope.confirmDialog = function(dttbModel) {
            var subScope = scope.$new();
            subScope.hasDefinedConfirmDialog = true;
            subScope.confirmMessage = "This will override the current configuration. Are you sure?";
            subScope.confirmDialog = function() {
                try {
                    dttbModel = eval("(" + dttbModel + ")");
                    helperService.decode(dttbModel);
                    helperService.postDecode(dttbModel);
                    //builder.dttbModel = angular.copy(dttbModel);
                    applyModel(dttbModel);
                    ngDialog.closeAll();
                } catch (e) {
                    console.warn(e, 'Error while uploading code');
                    Notification.error('Error while uploading code');
                }
            };
            ngDialog.openConfirm({
                scope: subScope,
                template: '/views/modal-confirm.html',
            });
        };
        ngDialog.openConfirm({
            scope: scope,
            template: '/views/modal-upload-code.html',
        });
    }

    /**
     * @ngdoc method 
     * @name goToPage
     * @methodOf builderApp.controller:BuilderController
     *
     * @param {string} pageKey The key for each menu/page. 
     * 
     * @description
     * Method to navigate / switch between each page of the left panel navigation.
     * 
     */
    function goToPage(pageKey) {
        switch (pageKey) {
            case 'reading':
                builder.templateUrl = 'builder-3-reading.html';
                break;
            case 'creating':
                builder.templateUrl = 'builder-3-creating.html';
                break;
            case 'updating':
                builder.templateUrl = 'builder-3-updating.html';
                break;
            case 'deleting':
                builder.templateUrl = 'builder-3-deleting.html';
                break;
            case 'readingRequest':
                builder.templateUrl = 'builder-3-reading-request.html';
                break;
            case 'readingFilters':
                builder.templateUrl = 'builder-3-reading-filters.html';
                break;
            case 'creatingRequest':
                builder.templateUrl = 'builder-3-creating-request.html';
                break;
            case 'creatingFields':
                builder.templateUrl = 'builder-3-creating-fields.html';
                break;
        }
    }

    /**
     * @ngdoc method 
     * @name init
     * @methodOf builderApp.controller:BuilderController
     * 
     * @description
     * The initial method will be called to activate the firt menu/page.
     * 
     */
    function init() {
        builder.templateUrl = builder.menu[0].templateUrl;
        builder.menu[0].active = true;
    }

    /**
     * @ngdoc method 
     * @name previewResult
     * @methodOf builderApp.controller:BuilderController
     * 
     * @description
     * Method to show the data-tablar result demo.
     * It will encode the builder's configuration and
     * then call the data-tablar module to display
     * the data-tablar table.
     *
     * Menu 6 - Results
     */
    function previewResult() {
        try {
            var copyOfdttbModel = angular.copy(builder.dttbModel);
            helperService.encode(copyOfdttbModel);
            var generatedCodes, stringifyObject;
            generatedCodes = 'var configDataTablar = ';
            stringifyObject = JSON.stringify(copyOfdttbModel, function(key, value) {
                return (typeof value === 'function') ? value.toString() : value;
            });
            stringifyObject = JSON.parse(stringifyObject);

            generatedCodes = '{\n';

            generatedCodes += helperService.generateIteration(stringifyObject, '', 1);

            generatedCodes += '}';

            builder.configDataTablar = eval("(" + generatedCodes + ")");
        } catch (e) {
            console.warn(e);
            Notification.error('Error while previewing result');
        }
    }

    /**
     * @ngdoc method 
     * @name previewCodes
     * @methodOf builderApp.controller:BuilderController
     * 
     * @description
     * Method to show the preview source-code configuration.
     * It will process the encoding of all components of builder 
     * and produce the javascript object type of source-code configuration.
     *
     * The result will display inside a code editor. User can then copy
     * and use it.
     *
     * Menu 5 - Preview
     */
    function previewCodes() {
        var copyOfdttbModel = angular.copy(builder.dttbModel);
        helperService.encode(copyOfdttbModel);

        var generatedCodes, stringifyObject;

        generatedCodes = 'var configDataTablar = ';

        stringifyObject = JSON.stringify(copyOfdttbModel, function(key, value) {
            return (typeof value === 'function') ? value.toString() : value;
        });

        stringifyObject = JSON.parse(stringifyObject);

        generatedCodes += '{\n';

        generatedCodes += helperService.generateIteration(stringifyObject, '', 1);

        generatedCodes += '};';

        builder.generatedCodes = generatedCodes;
    }
}