'use strict';
/**
 * @ngdoc service
 * @name builderApp.service:helperService
 *
 * @requires ngDialog
 * @requires DTTB_BUILDER_PARAMETERS
 * 
 * @description
 * Service provides useful helper functions.
 */
angular.module('builderApp')
  .factory('helperService', ['ngDialog', 'DTTB_BUILDER_PARAMETERS', function(ngDialog, DTTB_BUILDER_PARAMETERS) {

    return {
      convertObjectToArray: convertObjectToArray,
      convertArrayToObject: convertArrayToObject,
      generateIteration: generateIteration,
      getVarType: getVarType,
      escapeQuoteString: escapeQuoteString,
      addBasicObjectToArray: addBasicObjectToArray,
      addToArrayWithTemplate: addToArrayWithTemplate,
      addToArray: addToArray,
      removeFromArray: removeFromArray,
      addToObject: addToObject,
      removeFromObject: removeFromObject,
      decode: decode,
      postDecode: postDecode,
      encode: encode,
      generateTabs: generateTabs,
      parseHostUrl: parseHostUrl
    };
    /**
     * @ngdoc method
     * @name generateTabs
     * @methodOf builderApp.service:helperService
     * 
     * @description
     * Helper function for dttbBuilderMenu directive
     * 
     * @param {number} dttbMenuLevel Level of menu
     * 
     * @returns {string} tab string.
     **/
    function generateTabs(dttbMenuLevel) {
      var tabs = '';
      for (var i = 1; i < dttbMenuLevel; i++) {
        tabs += '&nbsp;&nbsp;';
      }
      return tabs;
    }

    /**
     * @ngdoc method
     * @name getVarType
     * @methodOf builderApp.service:helperService
     * 
     * @description
     * Get type of variable
     * 
     * @param {var} obj variable whose type will be defined
     * 
     * @returns {string} type of variable
     **/
    function getVarType(obj) {
      return ({}).toString.call(obj).match(/\s([a-zA-Z]+)/)[1].toLowerCase();
    }

    /**
     * @ngdoc method
     * @name escapeQuoteString
     * @methodOf builderApp.service:helperService
     * 
     * @description
     * Escape quote of string
     * 
     * @param {text} str String to be escaped
     * 
     * @returns {string} Escaped string
     **/
    function escapeQuoteString(str) {
      return (str + '').replace(/[\\"']/g, '\\$&').replace(/\u0000/g, '\\0');
    }

    /**
     * @ngdoc method
     * @name generateIteration
     * @methodOf builderApp.service:helperService
     * 
     * @description
     * - Remove from object some dttbBuilder property: toggleOffPlaceholder, dttbBuilderActive.
     * - Generate javascript source code from dttbModel object.
     * - Handle for array, object, number, string and boolean.
     *
     * @param {object} list list
     * @param {string} generatedCodes generatedCodes
     * @param {number} level level
     * @param {boolean} asArray asArray
     * 
     * @returns {string} generatedCodes
     **/
    function generateIteration(list, generatedCodes, level, asArray) {
      var varType;
      angular.forEach(list, function(value, key) {
        varType = getVarType(value);

        for (var i = 0; i < level; i++) {
          generatedCodes += '\t';
        }
        if (!asArray) {
          if (angular.isString(key) && key.length > 0) {
            generatedCodes += key;
            generatedCodes += ': ';
          } else {
            return; //07/05 Prevent object property like: '' : '' => BAD
          }
        }
        var levelTabStrings = '';
        for (var i = 0; i < level; i++) {
          levelTabStrings += '\t';
        }
        switch (varType) {
          case 'array':
            generatedCodes += generateIteration(value, '[\n', level + 1, true);
            generatedCodes += levelTabStrings + ']';
            break;
          case 'object':
            generatedCodes += generateIteration(value, '{\n', level + 1);
            generatedCodes += levelTabStrings + '}';
            break;
          case 'number':
            generatedCodes += value;
            break;
          case 'string':
            switch (key) {
              case 'conversionResponse':
              case 'conversionRequest':
                value = value.replace('anonymous', '');
                value = value.replace('\n/**/', '');
                generatedCodes += value;
                break;
              default:
                generatedCodes += "'" + escapeQuoteString(value) + "'";
                break;
            }

            break;
          case 'boolean':
            if (value) {
              generatedCodes += 'true';
            } else {
              generatedCodes += 'false';
            }
            break;
          default:
            break;
        }
        generatedCodes += ',';
        generatedCodes += '\n';
      });
      return generatedCodes;
    }

    /**
     * @ngdoc method
     * @name convertObjectToArray
     * @methodOf builderApp.service:helperService
     * 
     * @description
     * Convert an object to array of (key, value) pairs.
     * 
     * @param {object} object Object to be converted
     * 
     * @returns {array} Array result
     **/
    function convertObjectToArray(object) {
      var array = [];
      if (angular.isObject(object)) {
        angular.forEach(object, function(value, key) {
          array.push({
            key: key,
            value: value
          });
        });
      }
      return array;
    }

    /**
     * @ngdoc method
     * @name convertArrayToObject
     * @methodOf builderApp.service:helperService
     * 
     * @description
     * Convert array of (key, value) pairs to object
     * 
     * @param {array} array Array to be converted.
     * 
     * @returns {object} Object result
     **/
    function convertArrayToObject(array) {
      var object = {};
      if (_.isArray(array)) {
        angular.forEach(array, function(element) {
          if (_.has(element, 'key') && _.has(element, 'value')) {
            object[element.key] = element.value;
          }
        });
      }
      return object;
    }

    /**
     * @ngdoc method
     * @name addBasicObjectToArray
     * @methodOf builderApp.service:helperService
     * 
     * @description
     * Add a pair (key, value) to array
     * 
     * @param {array} arrayReference Reference of array.
     **/
    function addBasicObjectToArray(arrayReference) {
      addToArray(arrayReference, {
        key: '',
        value: '',
      });
    }

    /**
     * @ngdoc method
     * @name addToArrayWithTemplate
     * @methodOf builderApp.service:helperService
     * 
     * @description
     * Add a model template to array.
     * 
     * @param {array} arrayReference Array reference to be added.
     * @param {string} templateKey Template key
     **/
    function addToArrayWithTemplate(arrayReference, templateKey) {
      addToArray(arrayReference, DTTB_BUILDER_PARAMETERS[templateKey]);
    }

    /**
     * @ngdoc method
     * @name addToArray
     * @methodOf builderApp.service:helperService
     * 
     * @description
     * Add value to array.
     * 
     * @param {array} arrayReference Array reference to be added.
     * @param {any} valueToAdd Value to add.
     **/
    function addToArray(arrayReference, valueToAdd) {
      if (!angular.isArray(arrayReference)) {
        arrayReference = [];
      }
      arrayReference.push(angular.copy(valueToAdd));
    }
    /**
     * @ngdoc method
     * @name parseHostUrl
     * @methodOf builderApp.service:helperService
     * 
     * @description
     * Get the host and path of a url
     * 
     * @param {string} urlString Url to be parsed.
     *
     * @returns {string} The host of that url
     **/
    function parseHostUrl(urlString) {
      var host = urlString.split('//');
      host = host[1] || '';
      if (host.length > 0) {
        host = host.split('/');
        host = host[0] || '';
      }
      return host;
    }

    /**
     * @ngdoc method
     * @name removeFromArray
     * @methodOf builderApp.service:helperService
     * 
     * @description
     * 
     * @param {array} arrayReference Array reference.
     * @param {number} indexToRemove Position to be removed.
     **/
    function removeFromArray(arrayReference, indexToRemove) {
      ngDialog.openConfirm({
        template: '/views/modal-confirm.html',
      }).then(function() {
        arrayReference.splice(indexToRemove, 1);
      });
    }

    /**
     * @ngdoc method
     * @name addToObject
     * @methodOf builderApp.service:helperService
     * 
     * @description
     * Add new property to object.
     * 
     * @param {object} objectReference Object
     * @param {string} propertyName Propery Name
     * @param {any} valueToAdd Property Value
     **/
    function addToObject(objectReference, propertyName, valueToAdd) {
      if (!objectReference) {
        objectReference = {};
      }
      if (objectReference.hasOwnProperty(propertyName)) {
        ngDialog.openConfirm({
          template: '/views/modal-confirm.html', //specific message for overidding object properties.
        }).then(function() {
          objectReference[propertyName] = angular.copy(valueToAdd);
        });
      } else {
        objectReference[propertyName] = angular.copy(valueToAdd);
      }
    }

    /**
     * @ngdoc method
     * @name removeFromObject
     * @methodOf builderApp.service:helperService
     * 
     * @description
     * Delete property of object
     * 
     * @param {object} objectReference Object reference.
     * @param {string} propertyToRemove Property to be removed.
     **/
    function removeFromObject(objectReference, propertyToRemove) {
      ngDialog.openConfirm({
        template: '/views/modal-confirm.html',
      }).then(function() {
        delete objectReference[propertyToRemove];
      });
    }

    /**
     * @ngdoc method
     * @name encode
     * @methodOf builderApp.service:helperService
     * 
     * @description
     * Encode the model of builder to 
     * make a javascript object configuration
     * and adapt to the data-tablar module.
     * 
     * @param {object} obj object to be encoded.
     **/
    function encode(obj) {
      for (var key in obj) {
        // skip loop if the property is from prototype
        //if (!validation_messages.hasOwnProperty(key)) continue;
        if (obj[key] === DTTB_BUILDER_PARAMETERS.toggleOffPlaceholder || key === 'dttbBuilderActive') {
          delete obj[key];
          continue;
        } else {
          if (key === 'conversionResponseBody') {
            var readingconversionResponse = Function("response", obj[key]);
            obj.conversionResponse = readingconversionResponse;
            delete obj.conversionResponseBody;
          } else if (key === 'conversionRequestBody') {
            var readingconversionRequest = Function("requestConfig", obj[key]);
            obj.conversionRequest = readingconversionRequest;
            delete obj.conversionRequestBody;
          } else if (key === 'data' || key === 'params' || key === 'headers') {
            if (angular.isArray(obj[key])) {
              obj[key] = convertArrayToObject(angular.copy(obj[key]));
            }
          } else if (key === 'options') {
            try {
              obj[key] = JSON.parse(obj[key]);
            } catch (e) {
              //obj[key] = angular.copy(DTTB_BUILDER_PARAMETERS.tempNumRowsPerPageOptions);
              console.warn(e, 'Error encode options');
            }
          } else if (key === 'numRowsPerPageOptions') {
            try {
              obj[key] = JSON.parse("[" + obj[key] + "]");
            } catch (e) {
              obj[key] = angular.copy(DTTB_BUILDER_PARAMETERS.tempNumRowsPerPageOptions);
              console.warn(e, 'Error encode numRowsPerPageOptions. Use default parameter tempNumRowsPerPageOptions');
            }
          } else if (angular.isObject(obj[key])) {
            encode(obj[key]);
          }
        }
      }
    }

    /**
     * @ngdoc method
     * @name decode
     * @methodOf builderApp.service:helperService
     * 
     * @description
     * Decode a configuration object to make it adapt to the builder.
     * Change conversionResponse -> conversionResponseBody. 
     * Change conversionRequest -> conversionRequestBody.
     * Convert object to array: data; params; headers.
     * Handle for options.
     * Handle for numRowsPerPageOptions.
     *
     * This is a recursive function.
     * 
     * @param {object} obj Object to be decoded.
     **/
    function decode(obj) {
      var temp;
      for (var key in obj) {
        if (key === 'conversionResponse') {
          temp = obj.conversionResponse.toString();
          obj.conversionResponseBody = temp.substring(temp.indexOf("{") + 1, temp.lastIndexOf("}"));
          delete obj.conversionResponse;
        } else if (key === 'conversionRequest') {
          temp = obj.conversionRequest.toString();
          obj.conversionRequestBody = temp.substring(temp.indexOf("{") + 1, temp.lastIndexOf("}"));
          delete obj.conversionRequest;
        } else if (key === 'data' || key === 'params' || key === 'headers') {
          if (angular.isObject(obj[key])) {
            obj[key] = convertObjectToArray(angular.copy(obj[key]));
          }
        } else if (key === 'options') {
          obj[key] = JSON.stringify(obj[key]);
        } else if (key === 'numRowsPerPageOptions') {
          obj[key] = obj[key].toString();
        } else if (angular.isObject(obj[key])) {
          decode(obj[key]);
        }
      }
    }

    /**
     * @ngdoc method
     * @name postDecode
     * @methodOf builderApp.service:helperService
     * 
     * @description
     * Post-processing for decoding dttbModel.
     * Extend default template for each property of dttbModel.
     * 
     * @param {object} reference of dttbModel model of builder after decoding
     **/
    function postDecode(dttbModel) {

      if (!angular.isObject(dttbModel)) {
        return;
      }
      //handle step by step each field
      //From inside out. 
      //Should have a mechanism to match the keywords with its default value template.

      //table
      dttbModel.table = angular.extend({}, DTTB_BUILDER_PARAMETERS.tempDttbModel.table, dttbModel.table);

      //optionAjaxRequests
      if (dttbModel.hasOwnProperty('optionAjaxRequests')) {
        if (angular.isArray(dttbModel.optionAjaxRequests)) {
          angular.forEach(dttbModel.optionAjaxRequests, function(optionAjaxRequest, key) {
            optionAjaxRequest.dttbBuilderActive = false;
            //optionAjaxRequest
            dttbModel.optionAjaxRequests[key] = angular.extend({}, DTTB_BUILDER_PARAMETERS.tempOptionAjaxRequest, optionAjaxRequest);
            //optionAjaxRequest.requestConfig
            dttbModel.optionAjaxRequests[key].requestConfig = angular.extend({},
              DTTB_BUILDER_PARAMETERS.tempOptionAjaxRequest.requestConfig,
              optionAjaxRequest.requestConfig
            );
          });
        } else {
          dttbModel.optionAjaxRequests = angular.copy(DTTB_BUILDER_PARAMETERS.tempDttbModel.optionAjaxRequests);
        }
      } else {
        dttbModel.optionAjaxRequests = angular.copy(DTTB_BUILDER_PARAMETERS.tempDttbModel.optionAjaxRequests);
      }

      //columns
      if (dttbModel.hasOwnProperty('columns')) {
        if (angular.isArray(dttbModel.columns)) {
          angular.forEach(dttbModel.columns, function(column, key) {
            column.dttbBuilderActive = false;
            //column
            dttbModel.columns[key] = angular.extend({}, DTTB_BUILDER_PARAMETERS.tempColForm, column);
            //column.formTypeConfig
            dttbModel.columns[key].formTypeConfig = angular.extend({}, DTTB_BUILDER_PARAMETERS.tempColForm.formTypeConfig, column.formTypeConfig);
            //column.displayAsConfig
            dttbModel.columns[key].displayAsConfig = angular.extend({}, DTTB_BUILDER_PARAMETERS.tempColForm.displayAsConfig, column.displayAsConfig);
          });
        } else {
          dttbModel.columns = angular.copy(DTTB_BUILDER_PARAMETERS.tempDttbModel.columns);
        }
      } else {
        dttbModel.columns = angular.copy(DTTB_BUILDER_PARAMETERS.tempDttbModel.columns);
      }

      var actions = dttbModel.actions;
      //reading

      var reading = actions.reading;
      //reading.requestConfig
      reading.requestConfig = angular.extend({}, DTTB_BUILDER_PARAMETERS.tempHttpRequest, reading.requestConfig);

      //reading.filters
      if (reading.hasOwnProperty('filters')) {
        if (angular.isArray(reading.filters)) {
          angular.forEach(reading.filters, function(filter, key) {
            reading.filters[key] = angular.extend({}, DTTB_BUILDER_PARAMETERS.tempReadingFilter, filter);
          });
        } else {
          reading.filters = DTTB_BUILDER_PARAMETERS.toggleOffPlaceholder;
        }
      } else {
        reading.filters = DTTB_BUILDER_PARAMETERS.toggleOffPlaceholder;
      }
      //creating
      if (actions.hasOwnProperty('creating')) {
        var creating = actions.creating;
        if (angular.isObject(creating)) {
          //creating.requestConfig
          creating.requestConfig = angular.extend({}, DTTB_BUILDER_PARAMETERS.tempHttpRequest, creating.requestConfig);

          //creating.fields
          if (creating.hasOwnProperty('fields')) {
            if (angular.isArray(creating.fields)) {
              angular.forEach(creating.fields, function(field, key) {
                creating.fields[key] = angular.extend({}, DTTB_BUILDER_PARAMETERS.tempCreatingField, field);
                creating.fields[key].formTypeConfig = angular.extend({}, DTTB_BUILDER_PARAMETERS.tempCreatingField.formTypeConfig, field.formTypeConfig);
              });
            } else {
              creating.fields = DTTB_BUILDER_PARAMETERS.tempCreating.fields;
            }
          } else {
            creating.fields = DTTB_BUILDER_PARAMETERS.tempCreating.fields;
          }
        } else {
          actions.creating = DTTB_BUILDER_PARAMETERS.toggleOffPlaceholder;
        }
      } else {
        actions.creating = DTTB_BUILDER_PARAMETERS.toggleOffPlaceholder;
      }
      //actions.deleting
      if (actions.hasOwnProperty('deleting')) {
        var deleting = actions.deleting;
        if (angular.isObject(deleting)) {
          //deleting.requestConfig
          deleting.requestConfig = angular.extend({}, DTTB_BUILDER_PARAMETERS.tempHttpRequest, deleting.requestConfig);

        } else {
          actions.deleting = DTTB_BUILDER_PARAMETERS.toggleOffPlaceholder;
        }
      } else {
        actions.deleting = DTTB_BUILDER_PARAMETERS.toggleOffPlaceholder;
      }
      //actions.updating
      if (actions.hasOwnProperty('updating')) {
        var updating = actions.updating;
        if (angular.isObject(updating)) {
          if (updating.hasOwnProperty('onCell')) {
            //requestConfig
            updating.onCell.requestConfig = angular.extend({}, DTTB_BUILDER_PARAMETERS.tempHttpRequest, updating.onCell.requestConfig);
          }
          if (updating.hasOwnProperty('onRow')) {
            //requestConfig
            updating.onRow.requestConfig = angular.extend({}, DTTB_BUILDER_PARAMETERS.tempHttpRequest, updating.onRow.requestConfig);
          }
        } else {
          actions.updating = DTTB_BUILDER_PARAMETERS.toggleOffPlaceholder;
        }
      } else {
        actions.updating = DTTB_BUILDER_PARAMETERS.toggleOffPlaceholder;
      }
    }
  }]);