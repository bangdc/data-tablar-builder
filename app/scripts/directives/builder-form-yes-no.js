'use strict';
/**
 * @ngdoc directive
 * @name builderApp.directive:builderFormYesNo
 * @restrict 'A'
 * @element ANY
 * @scope
 * 
 * @param {boolean} builderFormModel Binding - required - '=builderFormModel'. 
 * Model for this form element.
 * 
 * @description
 * Display a yes-no toggle button for boolean variable..
 */
angular.module('builderApp')
    .directive('builderFormYesNo', [builderFormYesNo]);

function builderFormYesNo() {
    var directive = {
        templateUrl: 'views/builder-form-yes-no.html',
        restrict: 'A',
        scope: {
            model: '=builderFormModel',
        },
        controller: builderFormYesNoController
    };
    return directive;
}

/**
 * @ngdoc controller
 * @name builderApp.controller:builderFormYesNoController
 *
 * @description
 * # builderFormYesNo
 * Controller of directive builderFormYesNo
 */
builderFormYesNoController.$inject = [];

function builderFormYesNoController() {}