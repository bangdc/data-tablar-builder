'use strict';
/**
 * @ngdoc directive
 * @name builderApp.directive:builderHttpConfig
 * @restrict 'A'
 * @element ANY
 * @scope
 * 
 * @param {object} dttbHttpConfig Binding - required - '=dttbHttpConfig'. 
 * Model for this HTTP configuration form.
 * 
 * @description
 * Display a list of form elements for HTTP Configuration
 */
angular.module('builderApp')
    .directive('builderHttpConfig', [builderHttpConfig]);

function builderHttpConfig() {
    var directive = {
        templateUrl: 'views/builder-http-config.html',
        restrict: 'A',
        scope: {
            config: '=dttbHttpConfig',
        },
        controller: builderHttpConfigController
    };
    return directive;
}

/**
 * @ngdoc controller
 * @name builderApp.controller:builderHttpConfigController
 * 
 * @requires $scope
 * @requires helperService
 * 
 * @description
 * # builderHttpConfig
 * Controller of directive builderHttpConfig
 */
builderHttpConfigController.$inject = ['$scope', 'helperService'];

function builderHttpConfigController($scope, helperService) {
    $scope.addBasicObjectToArray = helperService.addBasicObjectToArray;
}