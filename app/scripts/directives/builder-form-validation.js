'use strict';
/**
 * @ngdoc directive
 * @name builderApp.directive:builderFormValidation
 * @restrict 'E'
 * @scope
 * 
 * @param {array} builderValidations Binding - required - '=builderValidations'. Array of validations model
 * @param {string} builderDataType Binding - required - '=builderDataType'. Data Type
 * 
 * @description
 * Display a form element for validations.
 */
angular.module('builderApp')
    .directive('builderFormValidation', [builderFormValidation]);

function builderFormValidation() {
    var directive = {
        templateUrl: 'views/builder-form-validation.html',
        restrict: 'E',
        scope: {
            validations: '=builderValidations',
            dataType: '=builderDataType'
        },
        controller: builderFormValidationController
    };
    return directive;
}

/**
 * @ngdoc controller
 * @name builderApp.controller:builderFormValidationController
 * 
 * @requires $scope
 * @requires builderApp.service:helperService
 *
 * @description
 * # builderFormValidation
 * Controller of directive builderFormValidation
 */
builderFormValidationController.$inject = ['$scope', 'helperService'];

function builderFormValidationController($scope, helperService) {
    $scope.addToObject = helperService.addToObject;
    $scope.removeFromObject = helperService.removeFromObject;
}