'use strict';
/**
 * @ngdoc directive
 * @name builderApp.directive:builderConvertToNumber
 * 
 * @description
 * Convert ng-model from string to number. For example in select form element.
 */
angular.module('builderApp')
    .directive('builderConvertToNumber', [builderConvertToNumber]);

function builderConvertToNumber() {
    var directive = {
        require: 'ngModel',
        link: function(scope, element, attrs, ngModel) {
            ngModel.$parsers.push(function(val) {
                return parseInt(val, 10);
            });
            ngModel.$formatters.push(function(val) {
                return '' + val;
            });
        }
    };
    return directive;
}