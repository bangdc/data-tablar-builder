'use strict';
/**
 * @ngdoc directive
 * @name builderApp.directive:dttbBuilderMenu
 * @restrict 'A'
 * @element ANY
 * @scope
 * 
 * @param {object} dttbMenu Binding - required - '='. 
 * @param {number} dttbMenuLevel Binding - optional - '=?'. 
 * @param {number} dttbRootLevel Attribute - optional - '@?'. 
 * 
 * @description
 * Display a left menu of builder.
 */
angular.module('builderApp')
    .directive('dttbBuilderMenu', ['helperService', dttbBuilderMenu]);

function dttbBuilderMenu(dttbMenuHelper) {
    var directive = {
        templateUrl: 'views/builder-menu.html',
        restrict: 'A',
        scope: {
            dttbMenu: '=',
            dttbMenuLevel: '=?',
            dttbRootLevel: '@?'
        },
        controller: dttbBuilderMenuController
    };

    return directive;
}

/**
 * @ngdoc controller
 * @name builderApp.controller:dttbBuilderMenuController
 * 
 * @requires $scope
 * @requires helperService
 * 
 * @description
 * # dttbBuilderMenu
 * Controller of directive dttbBuilderMenu
 */
dttbBuilderMenuController.$inject = ['$scope', 'helperService'];

function dttbBuilderMenuController($scope, helperService) {
    $scope.dttbRootLevel = ($scope.dttbRootLevel) ? true : false;
    $scope.$on('dttbBuilderToggleMenu', function() {
        for (var i = 0; i < $scope.dttbMenu.length; i++) {
            $scope.dttbMenu[i].active = false;
        }
        $scope.$broadcast('dttbBuilderMenuHidden');
    });
    $scope.$on('dttbBuilderMenuHidden', function() {
        for (var i = 0; i < $scope.dttbMenu.length; i++) {
            $scope.dttbMenu[i].active = false;
        }
    });
    if (!$scope.dttbMenuLevel) {
        $scope.dttbMenuLevel = 0;
    }
    $scope.dttbMenuLevel++;
    $scope.dttbMenuTabs = helperService.generateTabs($scope.dttbMenuLevel);
    $scope.toggleItem = function(item) {
        $scope.$emit('dttbBuilderToggleMenu');
        $scope.$emit('dttbBuilderMenuTemplateUrl', item.templateUrl);
        item.active = !item.active;
    };
}