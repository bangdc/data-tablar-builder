'use strict';
/**
 * @ngdoc directive
 * @name builderApp.directive:builderToggleVar
 * @restrict 'E'
 * @scope
 * 
 * @param {any} builderToggleModel Binding - required - '=builderToggleModel'. 
 * @param {string} builderToggleKey Binding - required - '@builderToggleKey'.
 * 
 * @description
 * Display a toggle button to enable/disable a propery/variable.
 */
angular.module('builderApp')
    .directive('builderToggleVar', [builderToggleVar]);

function builderToggleVar() {
    var directive = {
        templateUrl: 'views/builder-toggle-var.html',
        restrict: 'E',
        scope: {
            model: '=builderToggleModel',
            toggleKey: '@builderToggleKey',
        },
        controller: builderToggleVarController
    };
    return directive;
}

/**
 * @ngdoc controller
 * @name builderApp.controller:builderToggleVarController
 * 
 * @requires $scope
 * @requires builderApp.service:helperService
 * @requires ngDialog
 * @requires $rootScope
 *
 * @description
 * # builderToggleVar
 * Controller of directive builderToggleVar
 */

builderToggleVarController.$inject = ['$scope', 'helperService', 'ngDialog', '$rootScope'];

function builderToggleVarController($scope, helperService, ngDialog, $rootScope) {
    $scope.toggleConfig = angular.copy($rootScope.dttbBuilderParameters.toggles[$scope.toggleKey]);
    var columns;
    $scope.$on('return-columns', function(event, $columns) {
        columns = angular.copy($columns);
    });

    /**
     * @ngdoc 
     * @name toggle
     * @methodOf builderApp.controller:builderToggleVarController
     *
     * @description
     * Main method of builderApp.controller:builderToggleVarController
     * enable/disable a variable.
     */
    $scope.toggle = function() {
        if ($scope.model === $rootScope.dttbBuilderToggleOffPlaceholder) {
            var scope = $scope.$new();

            scope.confirmMessage = $scope.toggleConfig.enableMessage || 'Are you sure to enable this variable?';
            scope.hasDefinedConfirmDialog = true;
            scope.confirmDialog = function(ngDialogId) {
                scope.$parent.model = angular.copy($rootScope.dttbBuilderParameters[$scope.toggleConfig.enableDefaultValueTemplateKey]);
                ngDialog.close(ngDialogId);
            };
            ngDialog.openConfirm({
                scope: scope,
                template: '/views/modal-confirm.html',
            });
        } else {
            var scope = $scope.$new();
            scope.hasDefinedConfirmDialog = true;
            scope.confirmDialog = function(ngDialogId) {
                scope.$parent.model = angular.copy($rootScope.dttbBuilderToggleOffPlaceholder);
                ngDialog.close(ngDialogId);
            };

            scope.confirmMessage = $scope.toggleConfig.disableMessage || 'Are you sure to disable this variable?';

            ngDialog.openConfirm({
                scope: scope,
                template: '/views/modal-confirm.html',
            });
        }
    };
}