'use strict';
/**
 * @ngdoc directive
 * @name builderApp.directive:builderFormConversionResponse
 * @restrict 'A'
 * @element ANY
 * @scope
 * 
 * @param {string} builderConversionModel Binding - required - '=builderConversionModel'. 
 * Model for this form conversion response.
 * 
 * @description
 * Display a code editor for that user can use to write a conversion-response function.
 */
angular.module('builderApp')
    .directive('builderFormConversionResponse', [builderFormConversionResponse]);

function builderFormConversionResponse() {
    var directive = {
        templateUrl: 'views/builder-form-conversion-response.html',
        restrict: 'A',
        scope: {
            model: '=builderConversionModel',
        },
        controller: builderFormConversionResponseController
    };
    return directive;
}

/**
 * @ngdoc controller
 * @name builderApp.controller:builderFormConversionResponseController
 *
 * @description
 * # builderFormConversionResponse
 * Controller of directive builderFormConversionResponse
 */
builderFormConversionResponseController.$inject = [];

function builderFormConversionResponseController() {}