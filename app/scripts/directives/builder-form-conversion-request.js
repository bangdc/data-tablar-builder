'use strict';
/**
 * @ngdoc directive
 * @name builderApp.directive:builderFormConversionRequest
 * @restrict 'A'
 * @element ANY
 * @scope
 * 
 * @param {string} builderConversionModel Binding - required - '=builderConversionModel'. 
 * Model for this form conversion request.
 * 
 * @description
 * Display a code editor for that user can use to write a conversion-request function.
 */
angular.module('builderApp')
    .directive('builderFormConversionRequest', [builderFormConversionRequest]);

function builderFormConversionRequest() {
    var directive = {
        templateUrl: 'views/builder-form-conversion-request.html',
        restrict: 'A',
        scope: {
            model: '=builderConversionModel',
        },
        controller: builderFormConversionRequestController
    };
    return directive;
}

/**
 * @ngdoc controller
 * @name builderApp.controller:builderFormConversionRequestController
 *
 * @description
 * # builderFormConversionRequest
 * Controller of directive builderFormConversionRequest
 */

builderFormConversionRequestController.$inject = [];

function builderFormConversionRequestController() {}