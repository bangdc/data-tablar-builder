'use strict';
/**
 * @ngdoc directive
 * @name builderApp.directive:builderFormLabel
 * @restrict 'A'
 * @element ANY
 * @scope
 * 
 * @param {string} builderLabelKey Attribute - required - '@builderLabelKey'. 
 * The key in parameters for this label.
 * 
 * @description
 * Display a label for each form element.
 */
angular.module('builderApp')
    .directive('builderFormLabel', [builderFormLabel]);

function builderFormLabel() {
    var directive = {
        templateUrl: 'views/builder-form-label.html',
        restrict: 'A',
        scope: {
            labelKey: '@builderLabelKey',
        },
        controller: builderFormLabelController
    };
    return directive;
}

/**
 * @ngdoc controller
 * @name builderApp.controller:builderFormLabelController
 * 
 * @requires $scope
 * 
 * @description
 * # builderFormLabel
 * Controller of directive builderFormLabel
 */
builderFormLabelController.$inject = ['$scope'];

function builderFormLabelController($scope) {}