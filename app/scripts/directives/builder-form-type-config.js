'use strict';
/**
 * @ngdoc directive
 * @name builderApp.directive:builderFormTypeConfig
 * @restrict 'A'
 * @element ANY
 * @scope
 * 
 * @param {string} builderLabelKey Attribute - required - '@builderFormType'.
 * @param {object} builderFormTypeConfig Bind - required - '=builderFormTypeConfigModel'.
 * 
 * @description
 * Display a form type configuration
 */
angular.module('builderApp')
    .directive('builderFormTypeConfig', [builderFormTypeConfig]);

function builderFormTypeConfig() {
    var directive = {
        templateUrl: 'views/builder-form-type-config.html',
        restrict: 'A',
        scope: {
            formType: '@builderFormType',
            formTypeConfig: '=builderFormTypeConfigModel'
        },
        controller: builderFormTypeConfigController
    };
    return directive;
}

/**
 * @ngdoc controller
 * @name builderApp.controller:builderFormTypeConfigController
 * 
 * @description
 * # builderFormTypeConfig
 * Controller of directive builderFormTypeConfig
 */
builderFormTypeConfigController.$inject = [];

function builderFormTypeConfigController() {}