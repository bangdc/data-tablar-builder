'use strict';
/**
 * @ngdoc directive
 * @name builderApp.directive:builderModalHelper
 * @restrict 'E'
 * @scope
 * 
 * @param {string} helpKey Binding - required - '@'.
 * 
 * @description
 * Display a help dialog for a component.
 */
angular.module('builderApp')
    .directive('builderModalHelper', [builderModalHelper]);

function builderModalHelper() {
    var directive = {
        templateUrl: 'views/builder-modal-helper.html',
        restrict: 'E',
        scope: {
            helpKey: '@'
        },
        controller: builderModalHelperController
    };
    return directive;
}

/**
 * @ngdoc controller
 * @name builderApp.controller:builderModalHelperController
 * 
 * @requires $scope
 * @requires $rootScope
 * @requires ngDialog
 * @requires DTTB_BUILDER_LABELS
 *
 * @description
 * # builderModalHelper
 * Controller of directive builderModalHelper
 */
builderModalHelperController.$inject = ['$scope', '$rootScope', 'ngDialog', 'DTTB_BUILDER_LABELS'];

function builderModalHelperController($scope, $rootScope, ngDialog, DTTB_BUILDER_LABELS) {
    /**
     * @ngdoc 
     * @name displayHelp
     * @methodOf builderApp.controller:builderModalHelperController
     *
     * @description
     * Main method of builderModalHelperController.
     * Display a dialog help.
     */
    $scope.displayHelp = function() {
        var helpKey = $scope.helpKey;

        var scope = $rootScope.$new();
        scope.helpKey = $scope.helpKey;
        scope.version = DTTB_BUILDER_LABELS[helpKey].helpDefaultVersion;
        scope.feedbackTitle = DTTB_BUILDER_LABELS[helpKey].feedbackTitle;
        scope.label = DTTB_BUILDER_LABELS[helpKey].label;
        ngDialog.open({
            scope: scope,
            template: DTTB_BUILDER_LABELS[helpKey].helpTemplateUrl
        });
    };
}