'use strict';
/**
 * @ngdoc directive
 * @name builderApp.directive:builderShowOptionAjaxRequests
 * @restrict 'E'
 * @scope
 * 
 * @description
 * Show modal - list of all options ajax requests
 */
angular.module('builderApp')
    .directive('builderShowOptionAjaxRequests', [builderShowOptionAjaxRequests]);

function builderShowOptionAjaxRequests() {
    var directive = {
        templateUrl: 'views/builder-show-option-ajax-requests.html',
        restrict: 'E',
        scope: {},
        controller: builderShowOptionAjaxRequestsController
    };
    return directive;
}

/**
 * @ngdoc controller
 * @name builderApp.controller:builderShowOptionAjaxRequestsController
 * 
 * @requires $scope
 * @requires $rootScope
 * 
 * @description
 * # builderShowOptionAjaxRequests
 * Controller of directive builderShowOptionAjaxRequests
 */
builderShowOptionAjaxRequestsController.$inject = ['$scope', '$rootScope', 'ngDialog', 'helperService'];

function builderShowOptionAjaxRequestsController($scope, $rootScope, ngDialog, helperService) {
    $scope.showRequests = function() {
        var scope = $scope.$new();

        scope.shortenUrl = function(urlString) {
            var host = helperService.parseHostUrl(urlString);
            return host;
        };
        scope.viewRequest = function(requestIndex) {
            var viewRequestScope = $scope.$new();

            viewRequestScope.optionAjaxRequest = angular.copy($rootScope.dttbBuilderOptionAjaxRequests[requestIndex]);

            viewRequestScope.update = function(ngDialogId) {
                viewRequestScope.$emit('update-option-ajax-request', requestIndex, viewRequestScope.optionAjaxRequest);
                ngDialog.close(ngDialogId);
            };
            ngDialog.openConfirm({
                scope: viewRequestScope,
                template: '/views/modal-option-ajax-request-view.html',
                closeByDocument: false,
                showClose: false,
                closeByEscape: false
            });
        };
        scope.createRequest = function(ngDialogId) {
            var createRequestScope = $scope.$new();

            createRequestScope.optionAjaxRequest = angular.copy($rootScope.dttbBuilderParameters.tempOptionAjaxRequest);

            createRequestScope.create = function(ngDialogId) {
                createRequestScope.$emit('new-option-ajax-request', createRequestScope.optionAjaxRequest);
                ngDialog.close(ngDialogId);
            };

            ngDialog.openConfirm({
                scope: createRequestScope,
                template: '/views/modal-option-ajax-request-create.html',
                closeByDocument: false,
                showClose: false,
                closeByEscape: false
            });
        };
        ngDialog.openConfirm({
            scope: scope,
            template: '/views/modal-option-ajax-requests.html',
        });
    };
}