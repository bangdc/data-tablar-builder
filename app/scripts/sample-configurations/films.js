{
	optionAjaxRequests: [],
	table: {
		uniqueId: 'omdb-film-table',
		displaySelectRowsPage: false,
		displayPaging: true,
		displayShowingRow: true,
		label: 'Film Database',
		primaryLabel: '',
		displayNavHighlights: true,
		displayNavArrange: true,
		displayNavLocalSort: true,
		displayNavHideShow: true,
		displayNavLanguage: true,
		displayNavExport: true,
		displayNavPrint: true,
		displayNavRefresh: true,
		displayNavShare: true,
		numPagesDisplaying: 5,
		numRowsPerPage: 10,
	},
	columns: [{
		dataType: 'text',
		formType: '',
		headerDirection: 'horizontal',
		sortable: false,
		uneditable: true,
		RTLtext: false,
		allowLocalFilter: true,
		label: 'IMDB ID',
		tooltip: '',
		key: 'imdbID',
		displayKey: 'imdbID',
		displayAs: 'text',
		displayAsConfig: {
			textCase: 'uppercase',
		},
		formTypeConfig: {},
	}, {
		dataType: 'text',
		formType: '',
		headerDirection: 'horizontal',
		sortable: false,
		uneditable: true,
		RTLtext: false,
		allowLocalFilter: true,
		label: 'Title',
		tooltip: '',
		key: 'Title',
		displayKey: 'Title',
		displayAs: 'text',
		displayAsConfig: {
			textCase: 'capitalizeEach',
		},
		formTypeConfig: {},
	}, {
		dataType: 'number',
		formType: '',
		headerDirection: 'horizontal',
		sortable: false,
		uneditable: true,
		RTLtext: false,
		allowLocalFilter: true,
		label: 'Year',
		tooltip: '',
		key: 'Year',
		displayKey: 'Year',
		displayAs: 'number',
		displayAsConfig: {},
		formTypeConfig: {},
		highlights: [{
			type: 'bg-primary',
			condition: '>=',
			value: '2000',
		}, {
			type: 'bg-success',
			condition: '<',
			value: '2000',
		}, ],
	}, {
		dataType: 'text',
		formType: '',
		headerDirection: 'horizontal',
		sortable: false,
		uneditable: true,
		RTLtext: false,
		allowLocalFilter: false,
		label: 'Type',
		tooltip: '',
		key: 'Type',
		displayKey: 'Type',
		displayAs: 'text',
		displayAsConfig: {
			textCase: 'uppercase',
		},
		formTypeConfig: {},
		highlights: [{
			type: 'bg-primary',
			condition: '===',
			value: 'movie',
		}, {
			type: 'bg-info',
			condition: '===',
			value: 'series',
		}, {
			type: 'bg-warning',
			condition: '===',
			value: 'episode',
		}, ],
	}, {
		dataType: 'text',
		formType: '',
		headerDirection: 'horizontal',
		sortable: false,
		uneditable: true,
		RTLtext: false,
		allowLocalFilter: false,
		label: 'Poster',
		tooltip: '',
		key: 'Poster',
		displayKey: 'Poster',
		displayAs: 'image',
		displayAsConfig: {},
		formTypeConfig: {},
	}, ],
	actions: {
		reading: {
			requestConfig: {
				method: 'GET',
				url: 'http://www.omdbapi.com/',
				params: {
					r: 'json',
				},
			},
			filters: [{
				label: 'Keyword',
				placeholder: 'Search Keyword',
				key: 's',
			}, {
				label: 'Year',
				placeholder: 'Year of release',
				key: 'y',
			}, {
				label: 'Type',
				placeholder: 'Type of film: movie, series, episode',
				key: 'type',
				options: [
					'movie',
					'series',
					'episode',
				],
			}, ],
			conversionRequest: function(requestConfig) {
				// @param {object} response Response object from request. 

				// @returns {object} The final response 
				// Start your conversion function of response from the line below.
				var dttbFilters = JSON.parse(requestConfig.params.dttbFilters);
				angular.forEach(dttbFilters, function(filter, key) {
					var val = filter.value.toString();
					if (val.length > 0) {
						requestConfig.params[filter.field] = val;
					}
				});

				requestConfig.params['page'] = requestConfig.params.dttbPage;

				delete requestConfig.params.dttbPage;
				delete requestConfig.params.dttbNumRowsPerPage;
				delete requestConfig.params.dttbSortCol;
				delete requestConfig.params.dttbSortDir;
				delete requestConfig.params.dttbFilters;
				return requestConfig;
				// End your conversion function before this line.

			},
			conversionResponse: function(response) {
				// @param {object} response Response object from request. 

				// @returns {object} The final response 
				// Start your conversion function of response from the line below.
				var formattedResult = {};

				if (response.status === 200) {
					if (response.data.Response === 'True') {
						formattedResult.dttbSuccess = true;
						formattedResult.dttbResponse = {
							dttbRows: response.data.Search,
							dttbNumTotalRows: response.data.totalResults
						};
					} else {
						formattedResult.dttbSuccess = true;
						formattedResult.dttbResponse = {
							dttbRows: [],
							dttbNumTotalRows: 0
						};
					}
				} else {
					formattedResult.dttbSuccess = false;
					formattedResult.dttbResponse = "Some error happened";
				}

				return formattedResult;
				// End your conversion function before this line.

			},
		},
	},
}