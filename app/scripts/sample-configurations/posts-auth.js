{
	table: {
		uniqueId: 'dttb-posts-example',
		displaySelectRowsPage: true,
		displayPaging: true,
		displayShowingRow: true,
		label: 'BLOG POSTS AUTHENTICATION',
		primaryLabel: 'POST ID',
		displayNavHighlights: true,
		displayNavArrange: true,
		displayNavLocalSort: true,
		displayNavHideShow: true,
		displayNavLanguage: true,
		displayNavExport: true,
		displayNavPrint: true,
		displayNavRefresh: true,
		displayNavShare: true,
		numPagesDisplaying: 3,
		numRowsPerPage: 10,
		numRowsPerPageOptions: [
			10,
			20,
			30,
			40,
			50,
		],
		primaryKey: 'id',
	},
	optionAjaxRequests: [{
		requestConfig: {
			method: 'GET',
			url: 'https://data-tablar-api-auth.herokuapp.com/api/authors',
		},
		callAtInit: [],
		conversionResponse: function(response) {
			// @param {object} response Response object from request. 

			// @returns {object} The final response 
			// Start your conversion function of response from the line below.
			return {
				dttbSuccess: true,
				dttbResponse: {
					dttbOptions: response.data
				}
			};

			// End your conversion function before this line.

		},
	}, ],
	columns: [{
		dataType: 'number',
		formType: 'input',
		headerDirection: 'horizontal',
		sortable: true,
		uneditable: true,
		RTLtext: false,
		allowLocalFilter: true,
		label: 'POST ID',
		tooltip: 'POST ID',
		key: 'id',
		displayKey: 'id',
		displayAs: 'ordinal',
		displayAsConfig: {},
		formTypeConfig: {},
	}, {
		dataType: 'text',
		formType: 'input',
		headerDirection: 'horizontal',
		sortable: true,
		uneditable: false,
		RTLtext: false,
		allowLocalFilter: true,
		label: 'POST TITLE',
		tooltip: 'POST TITLE',
		key: 'title',
		displayKey: 'title',
		displayAs: 'text',
		displayAsConfig: {
			textCase: 'capitalizeEach',
		},
		formTypeConfig: {
			displayRequiredStar: true,
			placeholder: 'POST TITLE',
		},
		validations: {
			required: {
				message: 'This field is required',
				rule: true,
			},
			minLength: {
				message: 'The length of text must be greater than or equal to 30',
				rule: 30,
			},
			maxLength: {
				message: 'The length of text must be less than or equal to 50',
				rule: 50,
			},
		},
		highlights: [{
			type: 'text-primary h4 dttb-text-bold',
			condition: 'containing',
			value: 'Love',
		}, {
			type: 'text-info h4 dttb-text-bold',
			condition: 'containing',
			value: 'Hello',
		}, {
			type: 'bg-primary',
			condition: 'maxLength',
			value: '50',
		}, ],
	}, {
		dataType: 'text',
		formType: 'textarea',
		headerDirection: 'horizontal',
		sortable: true,
		uneditable: false,
		RTLtext: false,
		allowLocalFilter: true,
		label: 'POST CONTENT',
		tooltip: 'POST CONTENT',
		key: 'content',
		displayKey: 'content',
		displayAs: 'text',
		displayAsConfig: {
			limitToLength: 200,
			limitToBegin: 0,
		},
		formTypeConfig: {
			displayRequiredStar: true,
		},
		validations: {
			required: {
				message: 'This field is required',
				rule: true,
			},
			minLength: {
				message: 'The length of text must be greater than or equal to 200',
				rule: 200,
			},
		},
		highlights: [{
			type: 'bg-primary',
			condition: 'minLength',
			value: '200',
		}, {
			type: 'bg-info',
			condition: 'containing',
			value: 'datatablar',
		}, ],
	}, {
		dataType: 'text',
		formType: 'input',
		headerDirection: 'horizontal',
		sortable: true,
		uneditable: true,
		RTLtext: false,
		allowLocalFilter: true,
		label: 'POST AUTHOR',
		tooltip: 'POST AUTHOR',
		key: 'author',
		displayKey: 'author',
		displayAs: 'text',
		displayAsConfig: {},
		formTypeConfig: {},
		highlights: [{
			type: 'bg-primary',
			condition: '===',
			value: 'admin.istrator',
		}, {
			type: 'bg-success',
			condition: '===',
			value: 'bill.carroll',
		}, {
			type: 'bg-info',
			condition: '===',
			value: 'pablo.johnson',
		}, ],
	}, ],
	actions: {
		reading: {
			requestConfig: {
				method: 'GET',
				url: 'https://data-tablar-api-auth.herokuapp.com/api/posts/',
			},
			filters: [{
				label: 'POST ID',
				placeholder: '',
				key: 'id',
			}, {
				label: 'POST TITLE',
				placeholder: '',
				key: 'title',
			}, {
				label: 'POST AUTHOR',
				placeholder: '',
				key: 'author',
				typeaheadAjaxRequestIndex: 0,
			}, ],
			conversionRequest: function(requestConfig) {
				// @param {object} response Response object from request. 

				// @returns {object} The final response 
				// Start your conversion function of response from the line below.
				var dttbFilters = JSON.parse(requestConfig.params.dttbFilters);
				angular.forEach(dttbFilters, function(filter, key) {
					var val = filter.value.toString();
					if (val.length > 0) {
						requestConfig.params[filter.field] = val;
					}
				});
				requestConfig.params['_start'] = parseInt(requestConfig.params.dttbNumRowsPerPage) * (parseInt(requestConfig.params.dttbPage) - 1);
				requestConfig.params['_limit'] = parseInt(requestConfig.params.dttbNumRowsPerPage);
				requestConfig.params['_sort'] = requestConfig.params.dttbSortCol;
				requestConfig.params['_order'] = requestConfig.params.dttbSortDir.toUpperCase();

				delete requestConfig.params.dttbPage;
				delete requestConfig.params.dttbNumRowsPerPage;
				delete requestConfig.params.dttbSortCol;
				delete requestConfig.params.dttbSortDir;
				delete requestConfig.params.dttbFilters;

				return requestConfig;

				// End your conversion function before this line.

			},
			conversionResponse: function(response) {
				// @param {object} response Response object from request. 

				// @returns {object} The final response 
				// Start your conversion function of response from the line below.
				var formattedResponse = {
					dttbRows: response.data,
					dttbNumTotalRows: parseInt(response.headers('X-Total-Count'))
				};
				return {
					dttbResponse: formattedResponse,
					dttbSuccess: true
				};

				// End your conversion function before this line.

			},
		},
		creating: {
			fields: [{
				dataType: 'text',
				formType: 'input',
				formTypeConfig: {
					displayRequiredStar: true,
					placeholder: 'POST TITLE',
				},
				validations: {
					required: {
						message: 'This field is required',
						rule: true,
					},
					minLength: {
						message: 'The length of text must be greater than or equal to 30',
						rule: 30,
					},
					maxLength: {
						message: 'The length of text must be less than or equal to 50',
						rule: 50,
					},
				},
				key: 'title',
				label: 'POST TITLE',
			}, {
				dataType: 'text',
				formType: 'textarea',
				formTypeConfig: {
					displayRequiredStar: true,
				},
				validations: {
					required: {
						message: 'This field is required',
						rule: true,
					},
					minLength: {
						message: 'The length of text must be greater than or equal to 200',
						rule: 200,
					},
				},
				key: 'content',
				label: 'POST CONTENT',
			}, ],
			requestConfig: {
				method: 'POST',
				url: 'https://data-tablar-api-auth.herokuapp.com/api/posts',
			},
			conversionResponse: function(response) {
				// @param {object} response Response object from request. 

				// @returns {object} The final response 
				// Start your conversion function of response from the line below.

				if (response.status === 201) {
					return {
						dttbSuccess: true,
						dttbResponse: 'Create successfully'
					};
				} else {
					return {
						dttbSuccess: false,
						dttbResponse: 'Fail to create'
					};
				}
				// End your conversion function before this line.

			},
		},
		updating: {
			onCell: {
				requestConfig: {
					method: 'PATCH',
					url: 'https://data-tablar-api-auth.herokuapp.com/api/posts/:id',
				},
				enableRouteParams: true,
				conversionResponse: function(response) {
					// @param {object} response Response object from request. 

					// @returns {object} The final response 
					// Start your conversion function of response from the line below.

					var result = {};

					if (response.status === 200) {
						result.dttbSuccess = true;
						result.dttbResponse = "Update successfully";
					} else {
						result.dttbSuccess = false;
						result.dttbResponse = "Fail to update";
					}

					return result;
					// End your conversion function before this line.

				},
			},
			onRow: {
				requestConfig: {
					method: 'PATCH',
					url: 'https://data-tablar-api-auth.herokuapp.com/api/posts/:id',
				},
				enableRouteParams: true,
				conversionResponse: function(response) {
					// @param {object} response Response object from request. 

					// @returns {object} The final response 
					// Start your conversion function of response from the line below.
					var result = {};

					if (response.status === 200) {
						result.dttbSuccess = true;
						result.dttbResponse = "Update successfully";
					} else {
						result.dttbSuccess = false;
						result.dttbResponse = "Fail to update";
					}

					return result;

					// End your conversion function before this line.

				},
			},
		},
		deleting: {
			requestConfig: {
				method: 'DELETE',
				url: 'https://data-tablar-api-auth.herokuapp.com/api/posts/:id',
			},
			enableRouteParams: true,
			conversionResponse: function(response) {
				// @param {object} response Response object from request. 

				// @returns {object} The final response 
				// Start your conversion function of response from the line below.

				if (response.status === 200) {
					return {
						dttbSuccess: true,
						dttbResponse: 'Delete successfully'
					};
				} else {
					return {
						dttbSuccess: false,
						dttbResponse: 'Fail to delete'
					};
				}
				// End your conversion function before this line.

			},
		},
	},
}