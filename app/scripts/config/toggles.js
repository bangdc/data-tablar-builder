'use strict';
var simpleDescription = 'Click to enable or disable.';
var builderToggles = {
    validation: {
        description: simpleDescription,
        enableDefaultValueTemplateKey: 'tempValidation',
        enableMessage: '',
        disableMessage: ''
    },
    tableOtherLanguage: {
        description: simpleDescription,
        enableMessage: '',
        disableMessage: ''
    },
    columnHighlights: {
        description: simpleDescription,
        enableDefaultValueTemplateKey: 'tempColHighlights',
        enableMessage: '',
        disableMessage: ''
    },
    columnValidations: {
        description: simpleDescription,
        enableDefaultValueTemplateKey: 'tempColValidations',
        enableMessage: '',
        disableMessage: ''
    },
    conversionRequest: {
        description: simpleDescription,
        enableDefaultValueTemplateKey: 'tempConversionRequest',
        enableMessage: 'Do you want to enable this Conversion Request function?',
        disableMessage: 'Do you want to disable this Conversion Request function?'
    },
    conversionResponse: {
        description: simpleDescription,
        enableDefaultValueTemplateKey: 'tempConversionResponse',
        enableMessage: 'Do you want to enable this Conversion Response function?',
        disableMessage: 'Do you want to disable this Conversion Response function?'
    },
    formTypeConfigOptions: {
        description: simpleDescription,
        enableDefaultValueTemplateKey: 'tempFormTypeConfigOptions',
        enableMessage: '',
        disableMessage: ''
    },
    formTypeConfigOptionsAjaxRequest: {
        description: simpleDescription,
        enableDefaultValueTemplateKey: 'tempFormTypeConfigOptionsAjaxRequest',
        enableMessage: '',
        disableMessage: ''
    },
    creating: {
        description: simpleDescription,
        enableDefaultValueTemplateKey: 'tempCreating',
        enableMessage: 'Do you want to enable CREATING action?',
        disableMessage: 'Do you want to disable CREATING action? All CREATING properties will be erased.'
    },
    updating: {
        description: simpleDescription,
        enableDefaultValueTemplateKey: 'tempUpdating',
        enableMessage: 'Do you want to enable UPDATING action?',
        disableMessage: 'Do you want to disable UPDATING action? All UPDATING properties will be erased.'
    },
    deleting: {
        description: simpleDescription,
        enableDefaultValueTemplateKey: 'tempDeleting',
        enableMessage: 'Do you want to enable DELETING action?',
        disableMessage: 'Do you want to disable DELETING action? All DELETING properties will be erased.'
    },
    readingFilters: {
        description: simpleDescription,
        enableDefaultValueTemplateKey: 'tempReadingFilters',
        enableMessage: 'Do you want to enable READING FILTERS?',
        disableMessage: 'Do you want to disable READING FILTERS? All FILTERS properties will be erased.'
    },
    filterConditions: {
        description: simpleDescription,
        enableDefaultValueTemplateKey: 'tempReadingFilterConditions',
        enableMessage: '',
        disableMessage: ''
    },
    filterOptions: {
        description: simpleDescription,
        enableMessage: '',
        disableMessage: ''
    },
    filterTypeaheadAjaxRequest: {
        description: simpleDescription,
        enableMessage: '',
        disableMessage: ''
    },
    httpParams: {
        description: simpleDescription,
        enableDefaultValueTemplateKey: 'tempHttpParams',
        enableMessage: '',
        disableMessage: ''
    },
    httpHeaders: {
        description: simpleDescription,
        enableDefaultValueTemplateKey: 'tempHttpHeaders',
        enableMessage: '',
        disableMessage: ''
    },
    httpData: {
        description: simpleDescription,
        enableDefaultValueTemplateKey: 'tempHttpData',
        enableMessage: '',
        disableMessage: ''
    },
    httpXsrfHeaderName: {
        description: simpleDescription,
        enableDefaultValueTemplateKey: 'tempHttpXsrfHeaderName',
        enableMessage: '',
        disableMessage: ''
    },
    httpXsrfCookieName: {
        description: simpleDescription,
        enableDefaultValueTemplateKey: 'tempXsrfCookieName',
        enableMessage: '',
        disableMessage: ''
    },
    httpCache: {
        description: simpleDescription,
        enableDefaultValueTemplateKey: 'tempHttpCache',
        enableMessage: '',
        disableMessage: ''
    },
    httpWithCredentials: {
        description: simpleDescription,
        enableDefaultValueTemplateKey: 'tempHttpWithCredentials',
        enableMessage: '',
        disableMessage: ''
    },
    httpResponseType: {
        description: simpleDescription,
        enableDefaultValueTemplateKey: 'tempResponseType',
        enableMessage: '',
        disableMessage: ''
    },
    optionAjaxRequestCallAtInit: {
        description: simpleDescription,
        enableDefaultValueTemplateKey: 'tempOptionAjaxRequestCallAtInit',
        enableMessage: '',
        disableMessage: ''
    },
    optionAjaxRequestCallAtEvent: {
        description: simpleDescription,
        enableDefaultValueTemplateKey: 'tempOptionAjaxRequestCallAtEvent',
        enableMessage: '',
        disableMessage: ''
    },
    optionAjaxRequestCallInterval: {
        description: simpleDescription,
        enableDefaultValueTemplateKey: 'tempOptionAjaxRequestCallInterval',
        enableMessage: '',
        disableMessage: ''
    },
    optionAjaxRequestTypeaheadKey: {
        description: simpleDescription,
        enableDefaultValueTemplateKey: 'tempOptionAjaxRequestTypeaheadKey',
        enableMessage: '',
        disableMessage: ''
    },
    displayAsConfigTextcase: {
        description: simpleDescription,
        enableDefaultValueTemplateKey: 'tempDisplayAsConfigTextcase',
        enableMessage: '',
        disableMessage: ''
    },
    displayAsConfigLimitLength: {
        description: simpleDescription,
        enableDefaultValueTemplateKey: 'tempDisplayAsConfigLimitLength',
        enableMessage: '',
        disableMessage: ''
    },
    displayAsConfigLimitBegin: {
        description: simpleDescription,
        enableDefaultValueTemplateKey: 'tempDisplayAsConfigLimitBegin',
        enableMessage: '',
        disableMessage: ''
    },
    displayAsConfigCurrency: {
        description: simpleDescription,
        enableDefaultValueTemplateKey: 'tempDisplayAsConfigCurrency',
        enableMessage: '',
        disableMessage: ''
    },
    displayAsConfigFraction: {
        description: simpleDescription,
        enableDefaultValueTemplateKey: 'tempDisplayAsConfigFraction',
        enableMessage: '',
        disableMessage: ''
    },
    displayAsConfigDateFormat: {
        description: simpleDescription,
        enableDefaultValueTemplateKey: 'tempDisplayAsConfigDateFormat',
        enableMessage: '',
        disableMessage: ''
    },
};