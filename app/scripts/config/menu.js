'use strict';
var DTTB_BUILDER_MENU = [{
    title: '1. General Configuration',
    description: '',
    active: false,
    templateUrl: 'builder-1.html'
}, {
    title: '2. Columns Configuration',
    description: '',
    active: false,
    templateUrl: 'builder-2.html'
}, {
    title: '3. Actions Configuration',
    description: '',
    active: false,
    templateUrl: 'builder-3-actions.html',
    subs: [{
        title: '3.1 Reading',
        description: '',
        active: false,
        templateUrl: 'builder-3-reading.html',
        subs: [{
            title: 'a) HTTP Request Configuration',
            description: '',
            active: false,
            templateUrl: 'builder-3-reading-request.html'
        }, {
            title: 'b) Data Filters Configuration',
            description: '',
            active: false,
            templateUrl: 'builder-3-reading-filters.html'
        }]
    }, {
        title: '3.2 Creating',
        description: '',
        active: false,
        templateUrl: 'builder-3-creating.html',
        subs: [{
            title: 'a) HTTP Request Configuration',
            description: '',
            active: false,
            templateUrl: 'builder-3-creating-request.html'
        }, {
            title: 'b) Form Fields',
            description: '',
            active: false,
            templateUrl: 'builder-3-creating-fields.html'
        }]
    }, {
        title: '3.3 Updating',
        description: '',
        active: false,
        templateUrl: 'builder-3-updating.html'
    }, {
        title: '3.4 Deleting',
        description: '',
        active: false,
        templateUrl: 'builder-3-deleting.html'
    }]
}, {
    title: '4. Option Ajax Requests',
    description: '',
    active: false,
    templateUrl: 'builder-4-option-requests.html'
}, {
    title: '5. Preview',
    description: '',
    active: false,
    templateUrl: 'builder-5-preview.html',
}, {
    title: '6. Results',
    description: '',
    active: false,
    templateUrl: 'builder-6-results.html'
}];