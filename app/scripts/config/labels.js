'use strict';
var DTTB_BUILDER_LABELS = {
    //builder
    builderGeneralConfig: {
        label: '1 - General Configuration',
        feedbackTitle: 'builder-general-configuration',
        helpTemplateUrl: '/views/helps/builder-general-configuration.html',
        helpDefaultVersion: '1.0'
    },
    builderColumnsConfig: {
        label: '2 - Columns Configuration',
        feedbackTitle: 'builder-columns-configuration',
        helpTemplateUrl: '/views/helps/builder-columns-configuration.html',
        helpDefaultVersion: '1.0'
    },
    builderActionsConfig: {
        label: '3 - Actions Configuration',
        feedbackTitle: 'builder-actions-configuration',
        helpTemplateUrl: '/views/helps/builder-actions-configuration.html',
        helpDefaultVersion: '1.0'
    },
    builderActionReading: {
        label: '3.1 - Action Reading',
        feedbackTitle: 'builder-action-reading',
        helpTemplateUrl: '/views/helps/builder-action-reading.html',
        helpDefaultVersion: '1.0'
    },
    builderActionReadingRequest: {
        label: '3.1.a - HTTP Request Configuration',
        feedbackTitle: 'builder-action-reading-request',
        helpTemplateUrl: '/views/helps/builder-action-reading-request.html',
        helpDefaultVersion: '1.0'
    },
    builderActionReadingFilters: {
        label: '3.1.b - Filters',
        feedbackTitle: 'builder-action-reading-filters',
        helpTemplateUrl: '/views/helps/builder-action-reading-filters.html',
        helpDefaultVersion: '1.0'
    },
    builderActionCreating: {
        label: '3.2 - Action Creating',
        feedbackTitle: 'builder-action-creating',
        helpTemplateUrl: '/views/helps/builder-action-creating.html',
        helpDefaultVersion: '1.0'
    },
    builderActionCreatingRequest: {
        label: '3.2.a - HTTP Request Configuration',
        feedbackTitle: 'builder-action-creating-request',
        helpTemplateUrl: '/views/helps/builder-action-creating-request.html',
        helpDefaultVersion: '1.0'
    },
    builderActionCreatingFields: {
        label: '3.2.b - Form Fields',
        feedbackTitle: 'builder-action-creating-fields',
        helpTemplateUrl: '/views/helps/builder-action-creating-fields.html',
        helpDefaultVersion: '1.0'
    },
    builderActionUpdating: {
        label: '3.3 - Action Updating',
        feedbackTitle: 'builder-action-updating',
        helpTemplateUrl: '/views/helps/builder-action-updating.html',
        helpDefaultVersion: '1.0'
    },
    builderActionUpdatingCell: {
        label: '3.3.a - Cell Updating',
        feedbackTitle: 'builder-action-updating-cell',
        helpTemplateUrl: '/views/helps/builder-action-updating-cell.html',
        helpDefaultVersion: '1.0'
    },
    builderActionUpdatingRow: {
        label: '3.3.b - Row Updating',
        feedbackTitle: 'builder-action-updating',
        helpTemplateUrl: '/views/helps/builder-action-updating-row.html',
        helpDefaultVersion: '1.0'
    },
    builderActionDeleting: {
        label: '3.4 - Action Deleting',
        feedbackTitle: 'builder-action-deleting',
        helpTemplateUrl: '/views/helps/builder-action-deleting.html',
        helpDefaultVersion: '1.0'
    },
    builderOptionAjaxRequests: {
        label: '4 - Option Ajax Requests',
        feedbackTitle: 'builder-options-ajax',
        helpTemplateUrl: '/views/helps/builder-options-ajax.html',
        helpDefaultVersion: '1.0'
    },
    builderPreview: {
        label: '5 - Preview',
        feedbackTitle: 'builder-preview',
        helpTemplateUrl: '/views/helps/builder-preview.html',
        helpDefaultVersion: '1.0'
    },
    builderPreview: {
        label: '6 - Results',
        feedbackTitle: 'builder-results',
        helpTemplateUrl: '/views/helps/builder-result.html',
        helpDefaultVersion: '1.0'
    },
    builderUpload: {
        label: 'Upload your data-tablar configuration',
        feedbackTitle: 'builder-upload',
        helpTemplateUrl: '/views/helps/builder-upload.html',
        helpDefaultVersion: '1.0'
    },
    builderOpenRecent: {
        label: 'Open recent configurations',
        feedbackTitle: 'builder-open-recent',
        helpTemplateUrl: '/views/helps/builder-open-recent.html',
        helpDefaultVersion: '1.0'
    },
    builderSave: {
        label: 'Save this configuration to local storage',
        feedbackTitle: 'builder-save',
        helpTemplateUrl: '/views/helps/builder-save.html',
        helpDefaultVersion: '1.0'
    },

    //
    enableRouteParams: {
        label: 'Route Parameters',
        feedbackTitle: 'enable-route-params',
        helpTemplateUrl: '/views/helps/http-enable-route-params.html',
        helpDefaultVersion: '1.0'
    },

    //table general configuration
    tableOtherLanguage: {
        label: 'Other Language Label',
        feedbackTitle: 'table-other-language',
        helpTemplateUrl: '/views/helps/table-other-language.html',
        helpDefaultVersion: '1.0'
    },
    tableLabel: {
        label: 'Table Label',
        feedbackTitle: 'table-label',
        helpTemplateUrl: '/views/helps/table-label.html',
        helpDefaultVersion: '1.0'
    },
    tableUniqueId: {
        label: 'Table Unique Id',
        feedbackTitle: 'table-unique-id',
        helpTemplateUrl: '/views/helps/table-unique-id.html',
        helpDefaultVersion: '1.0'
    },
    tablePrimaryLabel: {
        label: 'Table Primary Label',
        helpTemplateUrl: '/views/helps/table-primary-label.html',
        helpDefaultVersion: '1.0'
    },
    //pagination
    tablePagination: {
        label: 'Table Pagination',
        feedbackTitle: 'table-pagination',
        helpTemplateUrl: '/views/helps/table-pagination.html',
        helpDefaultVersion: '1.0'
    },
    tablePaginationShowingNumRows: {
        label: 'Showing number of rows',
        helpTemplateUrl: '/views/helps/table-pagination-numrows.html',
        helpDefaultVersion: '1.0'
    },
    tablePaginationShowing: {
        label: 'Showing Pagination',
        helpTemplateUrl: '/views/helps/table-pagination-showing.html',
        helpDefaultVersion: '1.0'
    },
    tablePaginationShowingNumRowsSelect: {
        label: 'Showing select field for number of rows',
        helpTemplateUrl: '/views/helps/table-pagination-numrows-select.html',
        helpDefaultVersion: '1.0'
    },
    tablePaginationNumPagesDisplaying: {
        label: 'Number of displaying pages',
        helpTemplateUrl: '/views/helps/table-pagination-numpages-displaying.html',
        helpDefaultVersion: '1.0'
    },
    tablePaginationNumRowsPerPage: {
        label: 'Number of rows per page',
        helpTemplateUrl: '/views/helps/table-pagination-numrows-perpage.html',
        helpDefaultVersion: '1.0'
    },
    tablePaginationNumRowsPerPageOptions: {
        label: 'Number of rows per page options for selecting',
        helpTemplateUrl: '/views/helps/table-pagination-numrows-perpage-options.html',
        helpDefaultVersion: '1.0'
    },
    //navigation
    tableNavigation: {
        label: 'Table Navigation',
        feedbackTitle: 'table-navigation',
        helpTemplateUrl: '/views/helps/table-navigation.html',
        helpDefaultVersion: '1.0'
    },
    tableNavigationHighlight: {
        label: 'Highlights data button',
        feedbackTitle: 'table-highlight',
        helpTemplateUrl: '/views/helps/table-navigation-highlight.html',
        helpDefaultVersion: '1.0'
    },
    tableNavigationArrange: {
        label: 'Arrange columns button',
        feedbackTitle: 'table-arrange',
        helpTemplateUrl: '/views/helps/table-navigation-arrange.html',
        helpDefaultVersion: '1.0'
    },
    tableNavigationLocalSort: {
        label: 'Local sorting button',
        feedbackTitle: 'table-navigation',
        helpTemplateUrl: '/views/helps/table-navigation-sort.html',
        helpDefaultVersion: '1.0'
    },
    tableNavigationHideShow: {
        label: 'Hide/show columns button',
        feedbackTitle: 'table-hide-show-cols',
        helpTemplateUrl: '/views/helps/table-navigation-hide-show.html',
        helpDefaultVersion: '1.0'
    },
    tableNavigationLanguage: {
        label: 'Change language button',
        feedbackTitle: 'table-language',
        helpTemplateUrl: '/views/helps/table-navigation-language.html',
        helpDefaultVersion: '1.0'
    },
    tableNavigationExport: {
        label: 'Export button',
        feedbackTitle: 'table-export',
        helpTemplateUrl: '/views/helps/table-navigation-export.html',
        helpDefaultVersion: '1.0'
    },
    tableNavigationPrint: {
        label: 'Print button',
        feedbackTitle: 'table-print',
        helpTemplateUrl: '/views/helps/table-navigation-print.html',
        helpDefaultVersion: '1.0'
    },
    tableNavigationRefresh: {
        label: 'Refresh button',
        feedbackTitle: 'table-refresh',
        helpTemplateUrl: '/views/helps/table-navigation-refresh.html',
        helpDefaultVersion: '1.0'
    },
    tableNavigationShare: {
        label: 'Share button',
        feedbackTitle: 'table-share',
        helpTemplateUrl: '/views/helps/table-navigation-share.html',
        helpDefaultVersion: '1.0'
    },

    //column
    colLabel: {
        label: 'Label',
        feedbackTitle: 'col-label',
        helpTemplateUrl: '/views/helps/col-label.html',
        helpDefaultVersion: '1.0'
    },
    colDataType: {
        label: 'Data Type',
        feedbackTitle: 'col-data-type',
        helpTemplateUrl: '/views/helps/col-data-type.html',
        helpDefaultVersion: '1.0'
    },
    colRTLText: {
        label: 'RTL Text ',
        feedbackTitle: 'col-rtl-text',
        helpTemplateUrl: '/views/helps/col-rtl-text.html',
        helpDefaultVersion: '1.0'
    },
    colKey: {
        label: 'Model Key',
        feedbackTitle: 'col-key',
        helpTemplateUrl: '/views/helps/col-key.html',
        helpDefaultVersion: '1.0'
    },
    colHeaderDirection: {
        label: 'Header Direction',
        feedbackTitle: 'col-header-direction',
        helpTemplateUrl: '/views/helps/col-header-direction.html',
        helpDefaultVersion: '1.0'
    },
    colSortable: {
        label: 'Sortable',
        feedbackTitle: 'col-sortable',
        helpTemplateUrl: '/views/helps/col-sortable.html',
        helpDefaultVersion: '1.0'
    },
    colLocalFilter: {
        label: 'Local Filter',
        feedbackTitle: 'col-local-filter',
        helpTemplateUrl: '/views/helps/col-local-filter.html',
        helpDefaultVersion: '1.0'
    },
    colUneditable: {
        label: 'Un-editable',
        feedbackTitle: 'col-uneditable',
        helpTemplateUrl: '/views/helps/col-uneditable.html',
        helpDefaultVersion: '1.0'
    },
    //form
    formType: {
        label: 'Form Type',
        feedbackTitle: 'form-type',
        helpTemplateUrl: '/views/helps/form-type.html',
        helpDefaultVersion: '1.0'
    },
    formTypeConfig: {
        label: 'Form Configuration',
        feedbackTitle: 'form-config',
        helpTemplateUrl: '/views/helps/form-config.html',
        helpDefaultVersion: '1.0'
    },
    formTypeConfigRequiredStar: {
        label: 'Show Required Star',
        feedbackTitle: 'form-config-required-star',
        helpTemplateUrl: '/views/helps/form-config-required-star.html',
        helpDefaultVersion: '1.0'
    },
    formTypeConfigPlaceholder: {
        label: 'Placeholder',
        feedbackTitle: 'form-config-placeholder',
        helpTemplateUrl: '/views/helps/form-config-placeholder.html',
        helpDefaultVersion: '1.0'
    },
    formTypeConfigOptionEditable: {
        label: 'Option Editable',
        feedbackTitle: 'form-config-option-editable',
        helpTemplateUrl: '/views/helps/form-config-option-editable.html',
        helpDefaultVersion: '1.0'
    },
    formTypeConfigOptionsAjaxRequest: {
        label: 'Options Ajax Request Index',
        feedbackTitle: 'form-config-options-ajax-request',
        helpTemplateUrl: '/views/helps/form-config-options-ajax-request.html',
        helpDefaultVersion: '1.0'
    },
    formTypeConfigCallAtInit: {
        label: 'Call At Init',
        feedbackTitle: 'form-config-options-ajax-request',
        helpTemplateUrl: '/views/helps/form-config-options-ajax-request.html',
        helpDefaultVersion: '1.0'
    },
    formTypeConfigCallAtEvent: {
        label: 'Call At Each Event',
        feedbackTitle: 'form-config-options-ajax-request',
        helpTemplateUrl: '/views/helps/form-config-options-ajax-request.html',
        helpDefaultVersion: '1.0'
    },
    formTypeConfigCallInterval: {
        label: 'Minimum Interval',
        feedbackTitle: 'form-config-options-ajax-request',
        helpTemplateUrl: '/views/helps/form-config-options-ajax-request.html',
        helpDefaultVersion: '1.0'
    },
    formTypeConfigTypeaheadKey: {
        label: 'Typeahead Key',
        feedbackTitle: 'form-config-options-ajax-request',
        helpTemplateUrl: '/views/helps/form-config-options-ajax-request.html',
        helpDefaultVersion: '1.0'
    },
    formTypeConfigOptionModelKey: {
        label: 'Option Model Key',
        feedbackTitle: 'form-config-option-model-key',
        helpTemplateUrl: '/views/helps/form-config-option-model-key.html',
        helpDefaultVersion: '1.0'
    },
    formTypeConfigOptionDisplayKey: {
        label: 'Option Display Key',
        feedbackTitle: 'form-config-option-display-key',
        helpTemplateUrl: '/views/helps/form-config-option-display-key.html',
        helpDefaultVersion: '1.0'
    },
    formTypeConfigOptions: {
        label: 'Options',
        feedbackTitle: 'form-config-options',
        helpTemplateUrl: '/views/helps/form-config-options.html',
        helpDefaultVersion: '1.0'
    },
    formTypeConfigBooleanAs: {
        label: 'Form Boolean As',
        feedbackTitle: 'form-config-boolean-as',
        helpTemplateUrl: '/views/helps/col-form-boolean-as.html',
        helpDefaultVersion: '1.0'
    },
    //col display
    colDisplayKey: {
        label: 'Model Displaying Key',
        feedbackTitle: 'col-display-key',
        helpTemplateUrl: '/views/helps/col-display-key.html',
        helpDefaultVersion: '1.0'
    },
    colDisplayAs: {
        label: 'Display As',
        feedbackTitle: 'col-display-as',
        helpTemplateUrl: '/views/helps/col-display-as.html',
        helpDefaultVersion: '1.0'
    },
    colDisplayAsConfig: {
        label: 'Display As Configuration',
        feedbackTitle: 'col-display-as-config',
        helpTemplateUrl: '/views/helps/col-display-as-config.html',
        helpDefaultVersion: '1.0'
    },
    colDisplayAsConfigTextCase: {
        label: 'Text Case',
        feedbackTitle: 'col-display-config-textcase',
        helpTemplateUrl: '/views/helps/col-display-config-textcase.html',
        helpDefaultVersion: '1.0'
    },
    colDisplayAsConfigArrayAs: {
        label: 'Array As',
        feedbackTitle: 'col-display-config-arrayas',
        helpTemplateUrl: '/views/helps/col-display-config-arrayas.html',
        helpDefaultVersion: '1.0'
    },
    colDisplayAsConfigDateFormat: {
        label: 'Date Format',
        feedbackTitle: 'col-display-config-date-format',
        helpTemplateUrl: '/views/helps/col-display-config-date-format.html',
        helpDefaultVersion: '1.0'
    },
    colDisplayAsConfigFractionSize: {
        label: 'Fraction Size',
        feedbackTitle: 'col-display-config-fraction',
        helpTemplateUrl: '/views/helps/col-display-config-fraction.html',
        helpDefaultVersion: '1.0'
    },
    colDisplayAsConfigBooleanAs: {
        label: 'Display Boolean As',
        feedbackTitle: 'col-display-config-boolean-as',
        helpTemplateUrl: '/views/helps/col-form-boolean-as.html',
        helpDefaultVersion: '1.0'
    },
    colDisplayAsConfigCurrency: {
        label: 'Currency',
        feedbackTitle: 'col-display-config-currency',
        helpTemplateUrl: '/views/helps/col-display-config-currency.html',
        helpDefaultVersion: '1.0'
    },
    colDisplayAsConfigLimitToLength: {
        label: 'Limit To Length',
        feedbackTitle: 'col-display-config-limit',
        helpTemplateUrl: '/views/helps/col-display-config-limit.html',
        helpDefaultVersion: '1.0'
    },
    colDisplayAsConfigLimitToBegin: {
        label: 'Limit To Begin',
        feedbackTitle: 'col-display-config-limit',
        helpTemplateUrl: '/views/helps/col-display-config-limit.html',
        helpDefaultVersion: '1.0'
    },
    //col highlights
    colHighlights: {
        label: 'Highlights',
        feedbackTitle: 'col-highlights',
        helpTemplateUrl: '/views/helps/col-highlights.html',
        helpDefaultVersion: '1.0'
    },
    //col boolean
    booleanAs_default: {
        label: 'As Default',
    },
    booleanAs_icon: {
        label: 'As Icon',
    },
    booleanAs_text: {
        label: 'As Text',
    },
    booleanDisplayTrue: {
        label: 'True Value Display',
        feedbackTitle: 'col-form-boolean-display',
        helpTemplateUrl: '/views/helps/col-form-boolean-display.html',
        helpDefaultVersion: '1.0'
    },
    booleanDisplayFalse: {
        label: 'False Value Display',
        feedbackTitle: 'col-form-boolean-display',
        helpTemplateUrl: '/views/helps/col-form-boolean-display.html',
        helpDefaultVersion: '1.0'
    },

    //http
    httpMethod: {
        label: 'Method',
        feedbackTitle: 'http-method',
        helpTemplateUrl: '/views/helps/http-method.html',
        helpDefaultVersion: '1.0'
    },
    httpUrl: {
        label: 'Url',
        feedbackTitle: 'http-url',
        helpTemplateUrl: '/views/helps/http-url.html',
        helpDefaultVersion: '1.0'
    },
    httpParams: {
        label: 'Params',
        feedbackTitle: 'http-params',
        helpTemplateUrl: '/views/helps/http-params.html',
        helpDefaultVersion: '1.0'
    },
    httpHeaders: {
        label: 'Headers',
        feedbackTitle: 'http-headers',
        helpTemplateUrl: '/views/helps/http-headers.html',
        helpDefaultVersion: '1.0'
    },
    httpData: {
        label: 'Data',
        feedbackTitle: 'http-data',
        helpTemplateUrl: '/views/helps/http-data.html',
        helpDefaultVersion: '1.0'
    },
    httpCache: {
        label: 'Cache',
        feedbackTitle: 'http-cache',
        helpTemplateUrl: '/views/helps/http-cache.html',
        helpDefaultVersion: '1.0'
    },
    httpXsrfHeaderName: {
        label: 'Xsrf Header Name',
        feedbackTitle: 'http-xsrf-header-name',
        helpTemplateUrl: '/views/helps/http-xsrf-header-name.html',
        helpDefaultVersion: '1.0'
    },
    httpXsrfCookieName: {
        label: 'Xsrf Cookie Name',
        feedbackTitle: 'http-xsrf-cookie-name',
        helpTemplateUrl: '/views/helps/http-xsrf-cookie-name.html',
        helpDefaultVersion: '1.0'
    },
    httpWithCredentials: {
        label: 'With Credentials',
        feedbackTitle: 'http-with-credentials',
        helpTemplateUrl: '/views/helps/http-with-credentials.html',
        helpDefaultVersion: '1.0'
    },
    httpResponseType: {
        label: 'Response Type',
        feedbackTitle: 'http-response-type',
        helpTemplateUrl: '/views/helps/http-response-type.html',
        helpDefaultVersion: '1.0'
    },

    //validation
    formValidations: {
        label: 'Form Validations',
        feedbackTitle: 'form-validations',
        helpTemplateUrl: '/views/helps/form-validations.html',
        helpDefaultVersion: '1.0'
    },
    validation_greaterThan: {
        label: 'Greater Than',
        feedbackTitle: 'sponse-type',
        helpTemplateUrl: '/views/helps/form-validations.html',
        helpDefaultVersion: '1.0'
    },
    validation_lessThan: {
        label: 'Less Than',
        helpTemplateUrl: '/views/helps/form-validations.html',
        helpDefaultVersion: '1.0'
    },
    validation_equalTo: {
        label: 'Equal To',
        helpTemplateUrl: '/views/helps/form-validations.html',
        helpDefaultVersion: '1.0'
    },
    validation_not: {
        label: 'Not',
        helpTemplateUrl: '/views/helps/form-validations.html',
        helpDefaultVersion: '1.0'
    },
    validation_lessEqual: {
        label: 'Less than or equal to',
        helpTemplateUrl: '/views/helps/form-validations.html',
        helpDefaultVersion: '1.0'
    },
    validation_greaterEqual: {
        label: 'Greater than or equal to',
        helpTemplateUrl: '/views/helps/form-validations.html',
        helpDefaultVersion: '1.0'
    },
    validation_containing: {
        label: 'Containing',
        helpTemplateUrl: '/views/helps/form-validations.html',
        helpDefaultVersion: '1.0'
    },
    validation_minLength: {
        label: 'Minimum length',
        helpTemplateUrl: '/views/helps/form-validations.html',
        helpDefaultVersion: '1.0'
    },
    validation_maxLength: {
        label: 'Maximum length',
        helpTemplateUrl: '/views/helps/form-validations.html',
        helpDefaultVersion: '1.0'
    },
    validation_required: {
        label: 'Required',
        helpTemplateUrl: '/views/helps/form-validations.html',
        helpDefaultVersion: '1.0'
    },
    validation_dateSince: {
        label: 'Date Since',
        helpTemplateUrl: '/views/helps/form-validations.html',
        helpDefaultVersion: '1.0'
    },
    validation_dateUntil: {
        label: 'Date Until',
        helpTemplateUrl: '/views/helps/form-validations.html',
        helpDefaultVersion: '1.0'
    },

    //filter
    filterLabel: {
        label: 'Label',
        feedbackTitle: 'filter-label',
        helpTemplateUrl: '/views/helps/filter-label.html',
        helpDefaultVersion: '1.0'
    },
    filterPlaceholder: {
        label: 'Placeholder',
        feedbackTitle: 'filter-placeholder',
        helpTemplateUrl: '/views/helps/filter-placeholder.html',
        helpDefaultVersion: '1.0'
    },
    filterKey: {
        label: 'Filter Key',
        feedbackTitle: 'filter-key',
        helpTemplateUrl: '/views/helps/filter-key.html',
        helpDefaultVersion: '1.0'
    },
    filterTypeahead: {
        label: 'Filter Value Typeahead',
        feedbackTitle: 'filter-typeahead',
        helpTemplateUrl: '/views/helps/filter-typeahead.html',
        helpDefaultVersion: '1.0'
    },
    filterOptions: {
        label: 'Filter Options',
        feedbackTitle: 'filter-options',
        helpTemplateUrl: '/views/helps/filter-typeahead.html',
        helpDefaultVersion: '1.0'
    },
    filterOptionsAjaxRequest: {
        label: 'Options Ajax Request Index',
        feedbackTitle: 'filter-options',
        helpTemplateUrl: '/views/helps/filter-typeahead.html',
        helpDefaultVersion: '1.0'
    },
    filterConditions: {
        label: 'Filter Conditions',
        feedbackTitle: 'filter-conditions',
        helpTemplateUrl: '/views/helps/filter-conditions.html',
        helpDefaultVersion: '1.0'
    },

    //creating fields
    creatingFieldLabel: {
        label: 'Label',
        feedbackTitle: 'creating-field-label',
        helpTemplateUrl: '/views/helps/creating-field-label.html',
        helpDefaultVersion: '1.0'
    },
    creatingFieldDataType: {
        label: 'Data Type',
        feedbackTitle: 'creating-field-data-type',
        helpTemplateUrl: '/views/helps/creating-field-data-type.html',
        helpDefaultVersion: '1.0'
    },
    creatingFieldFormType: {
        label: 'creatingFieldFormType',
        feedbackTitle: 'creating-field-form-type',
        helpTemplateUrl: '/views/helps/creating-field-form-type.html',
        helpDefaultVersion: '1.0'
    },
    creatingFieldKey: {
        label: 'Model Key',
        feedbackTitle: 'creating-field-key',
        helpTemplateUrl: '/views/helps/creating-field-key.html',
        helpDefaultVersion: '1.0'
    },
    //conversion
    conversionResponse: {
        label: 'Conversion Response',
        feedbackTitle: 'filter-conditions',
        helpTemplateUrl: '/views/helps/conversion-response.html',
        helpDefaultVersion: '1.0'
    },
    conversionRequest: {
        label: 'Conversion Request',
        feedbackTitle: 'filter-conditions',
        helpTemplateUrl: '/views/helps/conversion-request.html',
        helpDefaultVersion: '1.0'
    },
};