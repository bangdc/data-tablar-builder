'use strict';
var DTTB_BUILDER_TOGGLE_OFF_PLACEHOLDER = 'builderToggleOffPlaceholder';
var DTTB_BUILDER_TEMP_HTTP_REQUEST = {
    method: '',
    url: '',
    params: DTTB_BUILDER_TOGGLE_OFF_PLACEHOLDER,
    headers: DTTB_BUILDER_TOGGLE_OFF_PLACEHOLDER,
    data: DTTB_BUILDER_TOGGLE_OFF_PLACEHOLDER,
    xsrfHeaderName: DTTB_BUILDER_TOGGLE_OFF_PLACEHOLDER,
    xsrfCookieName: DTTB_BUILDER_TOGGLE_OFF_PLACEHOLDER,
    cache: DTTB_BUILDER_TOGGLE_OFF_PLACEHOLDER,
    timeout: DTTB_BUILDER_TOGGLE_OFF_PLACEHOLDER,
    withCredentials: DTTB_BUILDER_TOGGLE_OFF_PLACEHOLDER,
    responseType: DTTB_BUILDER_TOGGLE_OFF_PLACEHOLDER
};
var DTTB_BUILDER_PARAMETERS = {
    valueTrue: true,
    valueFalse: false,
    toggleOffPlaceholder: DTTB_BUILDER_TOGGLE_OFF_PLACEHOLDER,
    toggles: builderToggles,
    aceOption: {
        mode: 'javascript',
        theme: 'twilight'
    },
    tempNumRowsPerPageOptions: [5, 10, 15, 20],
    tempDttbModel: {
        optionAjaxRequests: [],
        table: {
            uniqueId: '',
            displaySelectRowsPage: false,
            displayPaging: false,
            displayShowingRow: false,
            label: '',
            primaryLabel: '',
            displayNavHighlights: true,
            displayNavArrange: true,
            displayNavLocalSort: true,
            displayNavHideShow: true,
            displayNavLanguage: true,
            displayNavExport: true,
            displayNavPrint: true,
            displayNavRefresh: true,
            displayNavShare: true,
            otherLanguageLabel: DTTB_BUILDER_TOGGLE_OFF_PLACEHOLDER
        },
        columns: [],
        actions: {
            creating: DTTB_BUILDER_TOGGLE_OFF_PLACEHOLDER,
            deleting: DTTB_BUILDER_TOGGLE_OFF_PLACEHOLDER,
            updating: DTTB_BUILDER_TOGGLE_OFF_PLACEHOLDER,
            reading: {
                requestConfig: angular.extend(angular.copy(DTTB_BUILDER_TEMP_HTTP_REQUEST), {
                    method: 'GET'
                }),
                filters: DTTB_BUILDER_TOGGLE_OFF_PLACEHOLDER,
                conversionRequest: DTTB_BUILDER_TOGGLE_OFF_PLACEHOLDER,
                conversionRequestBody: DTTB_BUILDER_TOGGLE_OFF_PLACEHOLDER,
                conversionResponse: DTTB_BUILDER_TOGGLE_OFF_PLACEHOLDER,
                conversionResponseBody: DTTB_BUILDER_TOGGLE_OFF_PLACEHOLDER
            }
        }
    },
    tempColForm: {
        dttbBuilderActive: true,

        dataType: 'text',
        formType: '',
        headerDirection: 'horizontal',
        sortable: true,
        uneditable: true,
        RTLtext: false,
        allowLocalFilter: true,
        label: '',
        tooltip: '',
        key: '',
        displayKey: '',
        displayAs: '',
        displayAsConfig: {
            textCase: DTTB_BUILDER_TOGGLE_OFF_PLACEHOLDER,
            limitToLength: DTTB_BUILDER_TOGGLE_OFF_PLACEHOLDER,
            limitToBegin: DTTB_BUILDER_TOGGLE_OFF_PLACEHOLDER,

            currency: DTTB_BUILDER_TOGGLE_OFF_PLACEHOLDER,
            fractionSize: DTTB_BUILDER_TOGGLE_OFF_PLACEHOLDER,

            dateFormat: DTTB_BUILDER_TOGGLE_OFF_PLACEHOLDER
        },
        formTypeConfig: {
            typeaheadAjaxRequestIndex: DTTB_BUILDER_TOGGLE_OFF_PLACEHOLDER,
            options: DTTB_BUILDER_TOGGLE_OFF_PLACEHOLDER,
            optionAjaxRequestIndex: DTTB_BUILDER_TOGGLE_OFF_PLACEHOLDER
        },
        validations: DTTB_BUILDER_TOGGLE_OFF_PLACEHOLDER,
        highlights: DTTB_BUILDER_TOGGLE_OFF_PLACEHOLDER
    },
    tempReadingFilter: {
        label: '',
        placeholder: '',
        key: '',
        conditionOptions: DTTB_BUILDER_TOGGLE_OFF_PLACEHOLDER,
        dttbBuilderActive: true,
        typeaheadAjaxRequestIndex: DTTB_BUILDER_TOGGLE_OFF_PLACEHOLDER,
        options: DTTB_BUILDER_TOGGLE_OFF_PLACEHOLDER,
    },
    tempReadingFilters: [],
    tempReadingFilterConditions: [],
    tempReadingFilterCondition: {
        label: '',
        value: '',
    },
    tempCreating: {
        fields: [],
        requestConfig: angular.extend({}, DTTB_BUILDER_TEMP_HTTP_REQUEST, {
            method: 'POST'
        }),
        conversionRequestBody: DTTB_BUILDER_TOGGLE_OFF_PLACEHOLDER,
        conversionResponseBody: DTTB_BUILDER_TOGGLE_OFF_PLACEHOLDER
    },
    tempCreatingField: {
        dataType: 'text',
        formType: 'input',
        formTypeConfig: {
            typeaheadAjaxRequestIndex: DTTB_BUILDER_TOGGLE_OFF_PLACEHOLDER,
            options: DTTB_BUILDER_TOGGLE_OFF_PLACEHOLDER,
            optionAjaxRequestIndex: DTTB_BUILDER_TOGGLE_OFF_PLACEHOLDER
        },
        validations: DTTB_BUILDER_TOGGLE_OFF_PLACEHOLDER,
        dttbBuilderActive: true
    },

    tempUpdating: {
        onCell: {
            requestConfig: angular.extend({}, DTTB_BUILDER_TEMP_HTTP_REQUEST, {
                method: 'PUT'
            }),
            conversionRequestBody: DTTB_BUILDER_TOGGLE_OFF_PLACEHOLDER,
            conversionResponseBody: DTTB_BUILDER_TOGGLE_OFF_PLACEHOLDER
        },
        onRow: {
            requestConfig: angular.extend({}, DTTB_BUILDER_TEMP_HTTP_REQUEST, {
                method: 'PUT'
            }),
            conversionRequestBody: DTTB_BUILDER_TOGGLE_OFF_PLACEHOLDER,
            conversionResponseBody: DTTB_BUILDER_TOGGLE_OFF_PLACEHOLDER
        }
    },
    tempOptionAjaxRequest: {
        requestConfig: angular.extend({}, DTTB_BUILDER_TEMP_HTTP_REQUEST, {
            method: 'GET'
        }),
        conversionRequestBody: DTTB_BUILDER_TOGGLE_OFF_PLACEHOLDER,
        conversionResponseBody: DTTB_BUILDER_TOGGLE_OFF_PLACEHOLDER,
        callAtInit: DTTB_BUILDER_TOGGLE_OFF_PLACEHOLDER,
        callAtEachEvent: DTTB_BUILDER_TOGGLE_OFF_PLACEHOLDER,
        callMinimumInterval: DTTB_BUILDER_TOGGLE_OFF_PLACEHOLDER,
        typeaheadKey: DTTB_BUILDER_TOGGLE_OFF_PLACEHOLDER,
    },
    tempOptionAjaxRequestCallAtInit: false,
    tempOptionAjaxRequestCallAtEvent: false,
    tempOptionAjaxRequestCallInterval: 20000,
    tempOptionAjaxRequestTypeaheadKey: '',
    tempDeleting: {
        requestConfig: angular.extend({}, DTTB_BUILDER_TEMP_HTTP_REQUEST, {
            method: 'DELETE'
        }),
        conversionRequestBody: DTTB_BUILDER_TOGGLE_OFF_PLACEHOLDER,
        conversionResponseBody: DTTB_BUILDER_TOGGLE_OFF_PLACEHOLDER
    },
    tempColHighlight: {
        type: '',
        condition: '',
        value: ''
    },
    tempColHighlights: [],
    tempColValidations: {},
    tempValidation: {
        rule: '',
        message: '',
    },
    displayAsTypes: ['image', 'url', 'html', 'text', 'number', 'email', 'boolean', 'ordinal', 'currency'],
    dataTypes: ['number', 'text', 'boolean', 'array', 'date'],
    formTypes: ['input', 'textarea', 'checkbox', 'radio', 'typeahead', 'selectSingle', 'selectMultiple', 'selectCascade', 'datePicker'],
    httpRequestTypes: ['GET', 'HEAD', 'POST', 'PUT', 'DELETE', 'JSONP', 'PATCH'],
    tempHttpRequest: angular.copy(DTTB_BUILDER_TEMP_HTTP_REQUEST),
    tempConversionResponse: '// @param {object} response Response object from request. \n\n// @returns {object} The final response \n// Start your conversion function of response from the line below.\n\n\n// End your conversion function before this line.\n',
    tempConversionRequest: '// @param {object} requestConfig The original HTTP object configuration. \n\n// @returns {object} The final HTTP object configuration \n// Start your conversion function of response from the line below.\n\n\n// End your conversion function before this line.\n',
    tempHttpParams: [],
    tempHttpHeaders: [],
    tempHttpData: [],
    tempHttpXsrfHeaderName: '',
    tempXsrfCookieName: '',
    tempHttpCache: false,
    tempHttpTimeout: 0,
    tempHttpWithCredentials: false,
    tempHttpResponseType: '',
    tempFormTypeConfigOptions: '', //array to string.
    tempFormTypeConfigOptionsAjaxRequest: 0, //Default the first request of option-ajax-requests array.
    tempDisplayAsConfigTextcase: 'capitalizeEach',
    tempDisplayAsConfigLimitBegin: 0,
    tempDisplayAsConfigLimitLength: 100,
    tempDisplayAsConfigCurrency: '$',
    tempDisplayAsConfigFraction: 0,
    tempDisplayAsConfigDateFormat: 'YYYY-MM-DD',
    dttbDataTypes: ['number', 'text', 'array', 'boolean', 'date'],
    dttbHeaderDirections: ['horizontal', 'vertical', 'diagonal'],
    dttbDisplayAs: {
        number: ['number', 'ordinal', 'currency'],
        text: ['text', 'url', 'email', 'image'],
        array: ['array'],
        boolean: ['boolean'],
        date: ['date']
    },
    dttbFormTypes: {
        number: ['input', 'selectSingle', 'radio'],
        text: ['input', 'textarea', 'selectSingle', 'typeahead', 'radio'],
        array: ['selectMultiple'],
        boolean: ['boolean'],
        date: ['datePicker']
    },
    dttbTextCases: [{
        value: 'uppercase',
        label: 'UPPERCASE'
    }, {
        value: 'lowercase',
        label: 'lowercase'
    }, {
        value: 'capitalizeEach',
        label: 'Capitalize Each Word'
    }],
    dttbArrayAs: ['list', 'text'],
    dttbHighlightConditions: {
        number: [{
            value: '>',
            label: 'Greater than'
        }, {
            value: '>=',
            label: 'Greater than or equal'
        }, {
            value: '<',
            label: 'Less than'
        }, {
            value: '<=',
            label: 'Less than or equal'
        }],
        text: [{
            value: 'minLength',
            label: 'Minimum length'
        }, {
            value: 'maxLength',
            label: 'Maximum length'
        }, {
            value: 'containing',
            label: 'Containing text'
        }],
        default: [{
            value: '===',
            label: 'Equal'
        }, {
            value: '!==',
            label: 'Not Equal'
        }]
    },
    dttbValidations: {
        number: [{
            type: 'number',
            forDataType: 'number',
            default: {
                message: 'The number must be greater than YOUR_RULE_HERE'
            },
            label: '[>] Greater Than',
            value: 'greaterThan'
        }, {
            type: 'number',
            forDataType: 'number',
            default: {
                message: 'The number must be greater than or equal to YOUR_RULE_HERE'
            },
            label: '[>=] Greater Than or Equal',
            value: 'greaterEqual'
        }, {
            type: 'number',
            forDataType: 'number',
            default: {
                message: 'The number must be equal to YOUR_RULE_HERE'
            },
            label: '[===] Equal To',
            value: 'equalTo'
        }, {
            type: 'number',
            forDataType: 'number',
            default: {
                message: 'The number must be not equal to YOUR_RULE_HERE'
            },
            label: '[!==] Not Equal To',
            value: 'not'
        }, {
            type: 'number',
            forDataType: 'number',
            default: {
                message: 'The number must be less than or equal to YOUR_RULE_HERE'
            },
            label: '[<=] Less Than or Equal',
            value: 'lessEqual'
        }, {
            type: 'number',
            forDataType: 'number',
            default: {
                message: 'The number must be less than YOUR_RULE_HERE'
            },
            label: '[<] Less Than',
            value: 'lessThan'
        }, ],
        text: [{
            type: 'text',
            forDataType: 'text',
            default: {
                message: 'The text must contain YOUR_RULE_HERE'
            },
            label: 'Containing',
            value: 'containing'
        }, {
            type: 'number',
            forDataType: 'text',
            default: {
                message: 'The length of text must be greater than or equal to YOUR_RULE_HERE'
            },
            label: 'Minimum Length',
            value: 'minLength'
        }, {
            type: 'number',
            forDataType: 'text',
            default: {
                message: 'The length of text must be less than or equal to YOUR_RULE_HERE'
            },
            label: 'Maximum Length',
            value: 'maxLength'
        }, ],
        date: [{
            type: 'text',
            forDataType: 'date',
            default: {
                message: 'The minimum date is'
            },
            label: 'Date Since',
            value: 'dateSince'
        }, {
            type: 'text',
            forDataType: 'date',
            default: {
                message: 'The maximum date is'
            },
            label: 'Date Until',
            value: 'dateUntil'
        }],
        default: [{
            default: {
                message: 'This field is required'
            },
            label: 'Required',
            value: 'required'
        }],
    },
    dttbHighlights: [{
        value: 'bg-primary',
        label: 'Background Primary'
    }, {
        value: 'bg-success',
        label: 'Background Success'
    }, {
        value: 'bg-info',
        label: 'Background Info'
    }, {
        value: 'bg-warning',
        label: 'Background Warning'
    }, {
        value: 'bg-danger',
        label: 'Background Danger'
    }, {
        value: 'text-primary h4 dttb-text-bold',
        label: 'Text Primary'
    }, {
        value: 'text-success h4 dttb-text-bold',
        label: 'Text Success'
    }, {
        value: 'text-info h4 dttb-text-bold',
        label: 'Text Info'
    }, {
        value: 'text-warning h4 dttb-text-bold',
        label: 'Text Warning'
    }, {
        value: 'text-danger h4 dttb-text-bold',
        label: 'Text Danger'
    }],
    validations: {
        greaterThan: {
            type: 'number',
            forDataType: 'number',
            default: {
                message: 'The number must be greater than YOUR_RULE_HERE'
            },
            label: '[>] Greater Than',
        },
        greaterEqual: {
            type: 'number',
            forDataType: 'number',
            default: {
                message: 'The number must be greater than or equal to YOUR_RULE_HERE'
            },
            label: '[>=] Greater Than or Equal',
        },
        equalTo: {
            type: 'number',
            forDataType: 'number',
            default: {
                message: 'The number must be equal to YOUR_RULE_HERE'
            },
            label: '[===] Equal To',
        },
        not: {
            type: 'number',
            forDataType: 'number',
            default: {
                message: 'The number must be not equal to YOUR_RULE_HERE'
            },
            label: '[!==] Not Equal To',
        },
        lessEqual: {
            type: 'number',
            forDataType: 'number',
            default: {
                message: 'The number must be less than or equal to YOUR_RULE_HERE'
            },
            label: '[<=] Less Than or Equal',
        },
        lessThan: {
            type: 'number',
            forDataType: 'number',
            default: {
                message: 'The number must be less than YOUR_RULE_HERE'
            },
            label: '[<] Less Than',
        },
        containing: {
            type: 'text',
            forDataType: 'text',
            default: {
                message: 'The text must contain YOUR_RULE_HERE'
            },
            label: 'Containing',
        },
        minLength: {
            type: 'number',
            forDataType: 'text',
            default: {
                message: 'The length of text must be greater than or equal to YOUR_RULE_HERE'
            },
            label: 'Minimum Length',
        },
        maxLength: {
            type: 'number',
            forDataType: 'text',
            default: {
                message: 'The length of text must be less than or equal to YOUR_RULE_HERE'
            },
            label: 'Maximum Length',
        },
        required: {
            default: {
                message: 'This field is required'
            },
            label: 'Required',
        },
        dateSince: {
            type: 'text',
            forDataType: 'date',
            default: {
                message: 'The minimum date is'
            },
            label: 'Date Since',
        },
        dateUntil: {
            type: 'text',
            forDataType: 'date',
            default: {
                message: 'The maximum date is'
            },
            label: 'Date Until',
        }
    },
};