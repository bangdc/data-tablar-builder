'use strict';

/**
 * @ngdoc overview
 * @name builderApp
 * @description
 * # builderApp
 *
 * Main module of the application.
 */
angular
    .module('builderApp', [
        'ngAnimate',
        'ngCookies',
        'ngResource',
        'ngRoute',
        'ngSanitize',
        'ngTouch',
        'ui.ace',
        'ngDialog',
        'LocalStorageModule',
        'dataTablar',
        'ui-notification',
        'ng-sortable'
    ])
    .constant('DTTB_BUILDER_MENU', DTTB_BUILDER_MENU)
    .constant('DTTB_BUILDER_PARAMETERS', DTTB_BUILDER_PARAMETERS)
    .constant('DTTB_BUILDER_LABELS', DTTB_BUILDER_LABELS)
    .config(['localStorageServiceProvider', function(localStorageServiceProvider) {
        localStorageServiceProvider.setPrefix('dataTablarBuilder');
        localStorageServiceProvider
            .setStorageType('localStorage');
    }])
    .config(function($routeProvider) {
        $routeProvider
            .when('/', {
                templateUrl: 'views/builder.html',
                controller: 'BuilderController',
                controllerAs: 'builder'
            })
            .when('/results', {
                templateUrl: 'views/results.html',
                controller: 'BuilderController',
                controllerAs: 'builder'
            })
            .otherwise({
                redirectTo: '/'
            });
    })
    .config(function(NotificationProvider) {
        NotificationProvider.setOptions({
            delay: 10000,
            startTop: 20,
            startRight: 10,
            verticalSpacing: 20,
            horizontalSpacing: 20,
            positionX: 'left',
            positionY: 'bottom'
        });
    })
    .run([
        '$rootScope',
        'ngDialog',
        'DTTB_BUILDER_PARAMETERS',
        'DTTB_BUILDER_LABELS',
        function(
            $rootScope,
            ngDialog,
            DTTB_BUILDER_PARAMETERS,
            DTTB_BUILDER_LABELS
        ) {
            $rootScope.dttbBuilderParameters = angular.copy(DTTB_BUILDER_PARAMETERS);
            $rootScope.dttbBuilderLabels = DTTB_BUILDER_LABELS;
            $rootScope.dttbBuilderAceOption = $rootScope.dttbBuilderParameters.aceOption;
            $rootScope.dttbBuilderToggleOffPlaceholder = DTTB_BUILDER_PARAMETERS.toggleOffPlaceholder;
            $rootScope.dttbBuilderHttpRequestTypes = angular.copy(DTTB_BUILDER_PARAMETERS.httpRequestTypes);

            $rootScope.dttbBuilderOpenFeedBackForm = function(feedbackTitle) {
                var scope = $rootScope.$new();
                scope.feedbackTitle = feedbackTitle;
                ngDialog.openConfirm({
                    scope: scope,
                    template: '/views/helps/modal-feedback.html',
                    closeByDocument: false,
                    closeByEscape: false
                });
            };
        }
    ]);